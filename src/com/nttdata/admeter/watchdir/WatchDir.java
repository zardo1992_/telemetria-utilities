package com.nttdata.admeter.watchdir;

import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.logging.log4j.Logger;

import com.nttdata.admeter.constant.ErrorCode;
import com.nttdata.admeter.interfaces.IConfigUpdate;
import com.nttdata.admeter.output.ErrorLogger;

/**
 * Class used to monitor the configuration file and the error dictionary, and to handle if these files are edited, deleted, or created.
 * @author BurrafatoMa
 *
 */
public class WatchDir extends Thread {

	private final WatchService watcher;
    private final Map<WatchKey,Path> keys;
    private final boolean recursive;
    private boolean trace = false;
    
    /* Variables used to avoid editors that perform a "double-edit", of a "delete-and-create" edit, or other false positives */
    
    private Timer timer = new Timer();
    long lastModi = 0;
    private boolean calledCREATEinTheMeantime = false;
    private boolean previouslyCalledDELETE = false;
    
    private IConfigUpdate configToUpdate;
    private Logger logger;
    private ErrorLogger errorLogger;

    @SuppressWarnings("unchecked")
    static <T> WatchEvent<T> cast(WatchEvent<?> event) {
        return (WatchEvent<T>)event;
    }
    
    public boolean isStopped() { 
    	return Thread.currentThread().isInterrupted();
    }
    
    /**
     * Register the given directory with the WatchService.
     * @param dir the directory to register.
     * @throws IOException if an I/O error occurs.
     */
    private void register(Path dir) throws IOException {
        WatchKey key = dir.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
        if (trace) {
            Path prev = keys.get(key);
            if (prev == null) {
                logger.debug("Registering dir: "+ dir);
            } else {
                if (!dir.equals(prev)) {
                	logger.debug("Updating dir: " + prev + " -> " + dir);
                }
            }
        }
        keys.put(key, dir);
    }
    
    /**
     * Register the given directory, and all its sub-directories, with the WatchService.
	 * @param start the root directory to register.
     * @throws IOException if an I/O error occurs.
     */
    private void registerAll(final Path start) throws IOException {
        // Register directory and sub-directories
        Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                throws IOException
            {
                register(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    /**
     * Creates a WatchService and registers the given directory.
     * @param dir the directory to register.
     * @param recursive true if the subdirectories have to be registered too.
     * @param config the module's configuration container.
     * @param logger the module's Log4j2 logger.
     * @param errorLogger the module's error logger.
     * @throws IOException if an I/O error occurs.
     */
    public WatchDir(Path dir, boolean recursive, IConfigUpdate config, Logger logger, ErrorLogger errorLogger, String name) throws IOException {
        setName(name);
    	this.watcher = FileSystems.getDefault().newWatchService();
        this.keys = new HashMap<WatchKey,Path>();
        this.recursive = recursive;
        this.configToUpdate = config;
        this.logger = logger;
        this.errorLogger = errorLogger;
               
        if (recursive) {
            this.logger.debug("Scanning " + dir + " ...");
            registerAll(dir);
            this.logger.debug("Done.");
        } else {
            register(dir);
        }

        // Enable trace after initial registration
        this.trace = true;
    }

    @Override
    public void run() {
    	processEvents();
    }
        
    /**
     * Process all events for keys queued to the watcher.
     */
    private synchronized void processEvents() {
    	lastModi = 0;
        while(!isStopped()) {

            // Wait for key to be signaled
            WatchKey key;
            try {
                key = watcher.take();
            } catch (InterruptedException x) {
                return;
            }
            if (key == null) { Thread.yield(); continue; }

            Path dir = keys.get(key);
            if (dir == null) {
            	logger.error("WatchKey " + key + "not recognized!");
                continue;
            }

            for (WatchEvent<?> event: key.pollEvents()) {
                final Kind<?> kind = event.kind();

                if (kind == OVERFLOW) {
                	Thread.yield();
                    continue;
                }

                // Context for directory entry event is the file name of entry
                WatchEvent<Path> ev = cast(event);
                Path name = ev.context();
                final Path child = dir.resolve(name);

                final Path changed = (Path) event.context();
                                
                if (changed.endsWith(configToUpdate.getConfigFileName())) {
                	
                	if(kind == ENTRY_CREATE){
                		calledCREATEinTheMeantime = true;
                		
                		if(previouslyCalledDELETE){
                			logger.info("ENTRY_MODIFY: file " + child.toFile().getAbsolutePath() + " has changed (current size: " + child.toFile().length() + "); reloading runtime configuration");
                			previouslyCalledDELETE = false;
                		} else {
                			logger.info(kind.name() + ": file " + child.toFile().getAbsolutePath() + " has been created.");
                        	errorLogger.write(ErrorCode.CONFIG_FILE_RECREATED, "[file:"+child.toFile().getAbsolutePath()+"]");
                		}

                    	if(configToUpdate.update()){
                        	lastModi = child.toFile().lastModified();
                    	} else {
                    		lastModi = 0L;
                    	}
                	} else if(kind == ENTRY_MODIFY){
                		
                		if(child.toFile().lastModified() > lastModi){
                			if(child.toFile().length() > 0){
                    			logger.info(kind.name() + ": file " + child.toFile().getAbsolutePath() + " has changed at " + child.toFile().lastModified() + " (current size: " + child.toFile().length() + "); reloading runtime configuration");
                    			if(configToUpdate.update()){
                                	lastModi = child.toFile().lastModified();
                            	} else {
                            		lastModi = 0L;	
                            	}	
                			} else {
                    			logger.debug(kind.name() + ": file " + child.toFile().getAbsolutePath() + " has changed (current size: " + child.toFile().length() + "); doing nothing");
                			}
                		} else {
                			logger.debug(kind.name() + ": file " + child.toFile().getAbsolutePath() + " has changed (last mod: " + child.toFile().lastModified() + ", previous last mod: "  + lastModi + "); doing nothing");
                		}
                		
                	} else if(kind == ENTRY_DELETE){
                		
                		this.timer.cancel(); // This will cancel the current task. if there is no active task, nothing happens
                	    this.timer = new Timer();
                	    calledCREATEinTheMeantime = false;
                	    
                	    TimerTask action = new TimerTask() {
                	        public void run() {
                	            logTheDelete(kind.name(), child.toFile().getAbsolutePath(), child.toFile().lastModified());
                	        }

                	    };
                	    
                	    previouslyCalledDELETE = true;

                	    this.timer.schedule(action, 1000); // This starts the task
                	}
                }
                
                // if directory is created, and watching recursively, then register it and its sub-directories
                if (recursive && (kind == ENTRY_CREATE)) {
                    try {
                        if (Files.isDirectory(child, NOFOLLOW_LINKS)) {
                            registerAll(child);
                        }
                    } catch (IOException x) {
                    	logger.error("Error registering directory " + child);
                    }
                }
            }

            // Reset key and remove from set if directory no longer accessible
            boolean valid = key.reset();
            if (!valid) {
                keys.remove(key);

                // If all directories are inaccessible
                if (keys.isEmpty()) {
                    break;
                }
            }
            Thread.yield();
        }
    }
    
    /**
     * Log the file deletion after a certain amount of time, in order to avoid editors performing a "delete-and-create" editing of the file.
     * @param eventName the event name.
     * @param filePath the file path.
     * @param lastModified the last epoch time the file has been modified.
     */
    private void logTheDelete(String eventName, String filePath, long lastModified){
    	if(!calledCREATEinTheMeantime){
        	logger.warn(eventName + ": file " + filePath + " has been deleted! The program will keep running with the previous configuration");
        	errorLogger.write(ErrorCode.CONFIG_FILE_DELETED, "[file:"+filePath+"]");
        	lastModi = lastModified;
    	}
    	previouslyCalledDELETE = false;
    }
    
}