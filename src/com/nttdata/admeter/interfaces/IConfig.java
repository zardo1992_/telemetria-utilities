package com.nttdata.admeter.interfaces;

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Interface used to load and access the application/module configuration.
 * 
 * @author BurrafatoMa
 * @version 1.0.0
 *
 */
public interface IConfig {

	public ConfigurationCounters load(String filename, String section) throws IOException;

	public void loadParameters(IConfig config);

	public String getString(String key);

	public int getInteger(String key);

	public long getLong(String key);

	public JSONArray getArray(String key);

	public void setArray(String key, JSONArray val);

	public Object getObject(String key);

	public void setObject(String key, JSONObject val);

	public void setString(String key, String value);

	public void setInt(String key, int value);

	public JSONObject asJson();

	public JSONObject getJSONObject(String key);

	public void setLong(String key, long value);

	public void remove(String key);
}
