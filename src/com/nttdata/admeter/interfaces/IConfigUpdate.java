package com.nttdata.admeter.interfaces;

/**
 * Interface used to update the application/module configuration.
 * @author BurrafatoMa
 * @version 1.0.0
 *
 */
public interface IConfigUpdate {

	public boolean update();

	public String getConfigFileName();
}
