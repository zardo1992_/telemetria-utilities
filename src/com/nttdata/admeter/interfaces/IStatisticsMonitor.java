package com.nttdata.admeter.interfaces;


/**
 * Interface used to update and access the application/module statistics.
 * @author BurrafatoMa
 * @version 1.0.0
 *
 */
public interface IStatisticsMonitor {

	int getInt(String key);
	
	void putInt(String key, int value);

	void incrementInt(String key);
	
	void incrementInt(String key, int number);

}
