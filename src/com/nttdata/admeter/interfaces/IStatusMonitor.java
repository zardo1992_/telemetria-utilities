package com.nttdata.admeter.interfaces;

import com.nttdata.admeter.constant.StatusTimestamp;

/**
 * Interface used to update and access the application/module status (run files).
 * @author BurrafatoMa
 * @version 1.0.0
 *
 */
public interface IStatusMonitor {

	void setStatus(String key, long timestamp);

	void setStatus(String key, StatusTimestamp st);
}
