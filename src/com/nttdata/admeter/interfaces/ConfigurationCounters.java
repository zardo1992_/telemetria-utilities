package com.nttdata.admeter.interfaces;

/**
 * Three counters indicating how many elements were loaded in the configuration (how many correctly loaded, how many with warnings, how many with errors).
 * @author BurrafatoMa
 *
 */
public class ConfigurationCounters {
	public int correct = 0;
	public int warning = 0;
	public int error = 0;
}
