package com.nttdata.admeter.output;


import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import com.nttdata.admeter.constant.ErrorCode;
import com.nttdata.admeter.constant.ErrorCode.Source;
import com.nttdata.admeter.constant.Output;
import com.nttdata.admeter.utils.OutputUtils;
import com.nttdata.admeter.utils.TimeUtils;

/**
 * A class describing the line that will be written in the intermediate output
 * file.
 * 
 * @author BurrafatoMa
 * @version 1.0.0
 */
public class OutputMessage {

	// TODO definire nuovo header per Collector e Parser di AdSmart

	private String timestamp;
	private String jsonString;
	private JSONObject json;
	private String identifier;
	private String eventName;
	private long epochTs;
	private String deviceType;
	private ErrorLogger errorLogger;
	private Long obsTime;
	
	public String getTimestamp() {
		return timestamp;
	}

	public String getJsonString() {
		return jsonString;
	}

	public void setJsonString(String json) {
		this.jsonString = json;
	}

	public JSONObject getJson() {
		return json;
	}

	public void setJson(JSONObject json) {
		this.json = json;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	
	public long getEpochTs() {
		return epochTs;
	}
	
	public void setEpochTs(long epochTs) {
		this.epochTs = epochTs;
	}
	
	public Long getObsTime() {
		return obsTime;
	}
	
	public void setObsTime(Long obsTime) {
		this.obsTime = obsTime;
	}	
	

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	/**
	 * Standard Object constructor. Retrieves a list of variables and stores
	 * them individually.
	 * 
	 * @param timestamp
	 *            the formatted timestamp of the creation of this entry.
	 * @param json
	 *            the JSON object retrieved from the client. It can be a not
	 *            valid json if this message is generated from the collector
	 *            module.
	 */
	public OutputMessage(String timestamp, String json) {
		super();
		this.timestamp = timestamp;
		this.jsonString = json;
	}

	/**
	 * Object constructor with additional fields used for printing the parser
	 * output file.
	 * 
	 * @param timestamp
	 *            the formatted timestamp of the creation of this entry.
	 * @param identifier
	 *            the identifier of the client. can be a SmartcardID, a
	 *            SubscriberID or a STB_SN identifier.
	 * @param json
	 *            the JSON object that has to be printed on output file when
	 *            parser's processing finishes.
	 * @param eventName
	 *            the name of the output event reported.
	 */
	public OutputMessage(String identifier, String eventName, long epochTs, Long obsTime, String json, ErrorLogger errorLogger) {
		super();
		this.epochTs = epochTs;
		this.identifier = identifier;
		this.jsonString = json;
		this.eventName = eventName;
		this.obsTime = obsTime;
		this.errorLogger = errorLogger;
	} 

	/**
	 * Constructor starting from a byte array. Receives an object formatted as a
	 * BerkeleyDB entry and parses it in the single variables of this object.
	 * 
	 * @param message
	 *            a line entry of the BerkeleyDB file.
	 * @param logger
	 *            the Log4j2 logger.
	 * @param errorLogger
	 *            the error logger.
	 */
	public OutputMessage(byte[] message, Logger logger, ErrorLogger errorLogger, String source) {
		try {
			String[] sections = OutputUtils.splitMessage(message);
			int secLength = source.equals(Source.ADSMART) ? 2 : 3;
			if (sections != null) {
				if (secLength == 2) {
					timestamp = sections[0];
					jsonString = sections[1];
				} else if (secLength == 3) {
					timestamp = sections[0];
					deviceType = sections[1];
					jsonString = sections[2];
				}
			} else {
				throw new Exception("Entry is null or empty or could not find separator " + Output.SEPARATOR);
			}
		} catch (Exception e) {
			String err = ((message == null) ? "" : new String(message, Charset.defaultCharset()));
			logger.error("Error parsing message: " + err);
			errorLogger.write(ErrorCode.MESSAGE_INVALID, "[exception:" + e.getMessage() + ",message:" + message + "]");
		}
	}

	/**
	 * Gets the class current data and returns it as a byte array.
	 * 
	 * @return the list of byte array of the current data.
	 */
	public List<byte[]> toByteArray() {
		List<byte[]> fields = new ArrayList<>();

		if (timestamp != null) {
			fields.add(timestamp.getBytes(Charset.forName("UTF-8")));
		}
		
		if (identifier != null) {
			fields.add(identifier.getBytes(Charset.forName("UTF-8")));
		}
		
		if (eventName != null) {
			fields.add(eventName.getBytes(Charset.forName("UTF-8")));
		}
		
		if (epochTs!=0) {
			fields.add(String.valueOf(TimeUtils.string2long(TimeUtils.long2minutes(epochTs, errorLogger, Output.TIMESTAMP_FORMAT), errorLogger)).getBytes(Charset.forName("UTF-8")));
		}
		
		if (deviceType!=null) {
			fields.add(deviceType.getBytes(Charset.forName("UTF-8")));
		}
		
		if (obsTime != null) {
			fields.add(String.valueOf(obsTime).getBytes(Charset.forName("UTF-8")));
		}
		
		if (jsonString != null) {
			fields.add(jsonString.getBytes(Charset.forName("UTF-8")));
		}
		

		return fields;
	}
}
