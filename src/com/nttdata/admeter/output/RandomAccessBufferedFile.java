package com.nttdata.admeter.output;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.Charset;

/**
 * Class extending RandomAccessFile with buffering, for performances reasons.
 */
public class RandomAccessBufferedFile extends RandomAccessFile {

	/**
	 * For efficiency reasons, we define a byte buffer instead of char buffer.
	 */
	private byte buffer[];
	private int BUF_SIZE = 0;
	private int buf_end = 0;
	private int buf_pos = 0;
	private long real_pos = 0;

	/**
	 * Constructor with an additional parameter to specify the size of the buffer.
	 * 
     * @param file the file object.
     * @param mode the access mode, as described in the superclass.
	 * @param bufsize the size of the byte buffer.
	 * @param initialOffset the initial offset, as retrieved from the offset file.
     * @exception  IllegalArgumentException  if the mode argument is not equal
     *               to one of <tt>"r"</tt>, <tt>"rw"</tt>, <tt>"rws"</tt>, or
     *               <tt>"rwd"</tt>
     * @exception FileNotFoundException
     *            if the mode is <tt>"r"</tt> but the given file object does
     *            not denote an existing regular file, or if the mode begins
     *            with <tt>"rw"</tt> but the given file object does not denote
     *            an existing, writable regular file and a new regular file of
     *            that name cannot be created, or if some other error occurs
     *            while opening or creating the file
     * @exception  SecurityException         if a security manager exists and its
     *               <code>checkRead</code> method denies read access to the file
     *               or the mode is "rw" and the security manager's
	 * 
	 */
	public RandomAccessBufferedFile(File file, String mode, int bufsize, long initialOffset) throws IOException {
		super(file, mode);
		if(initialOffset > 0){
			seek(initialOffset);
		}
		invalidate();
		BUF_SIZE = bufsize;
		buffer = new byte[BUF_SIZE];
	}

	/**
	 * Read the input file. It always reads from the buffer first.
	 * It overrides the native read method in the original class, which is never engaged until the buffer has run out of room.
	 * In that case, the fillBuffer method is called to fill in the buffer.
	 * This is necessary when the seek method moves the file pointer out of the buffer.
	 */
	public final int read() throws IOException{
		
		if(buf_pos >= buf_end) {
			if(fillBuffer() < 0)
				return -1;
		}
		
		if(buf_end == 0) {
			return -1;
		} else {
			return buffer[buf_pos++];
		}
	}
	
	/**
	 * Fills the buffer reading the file. The original native read is invoked. See {@link RandomAccessFile#read()}.
	 * @return the number of characters read.
	 * @throws IOException if the native read function throws an IOException.
	 */
	private int fillBuffer() throws IOException {
		int n = super.read(buffer, 0, BUF_SIZE);
		if(n >= 0) {
			real_pos +=n;
			buf_end = n;
			buf_pos = 0;
		}
		return n;
	}
	
	/**
	 * Used to indicate that the buffer no longer contains valid contents. This is necessary when the seek method moves the file pointer out of the buffer.
	 * The original native getFilePointer is invoked. See {@link RandomAccessFile#getFilePointer()}.
	 * @throws IOException if the native getFilePointer function throws an IOException.
	 */
	private void invalidate() throws IOException {
		buf_end = 0;
		buf_pos = 0;
		real_pos = super.getFilePointer();
	}

	/**
	 * Overrides the original parameterized read method. See {@link RandomAccessFile#read(byte[], int, int)}.
	 * If there is enough buffer, it will simply call {@link System#arraycopy(Object, int, Object, int, int)} to copy a portion of the buffer directly into the user-provided area.
	 */
	public int read(byte b[], int off, int len) throws IOException {
		int leftover = buf_end - buf_pos;
		if(len <= leftover) {
			System.arraycopy(buffer, buf_pos, b, off, len);
			buf_pos += len;
			return len;
		}
		for(int i = 0; i < len; i++) {
			int c = this.read();
			if(c != -1)
				b[off+i] = (byte)c;
			else {
				return i;
			}
		}
		return len;
	}

	/**
	 * Overrides the original getFilePointer method, in order to get advantage of the byte buffer. See {@link RandomAccessFile#getFilePointer()}.
	 */
	public long getFilePointer() throws IOException{
		long l = real_pos;
		return (l - buf_end + buf_pos) ;
	}

	/**
	 * Overrides the original seek method, in order to get advantage of the byte buffer. See {@link RandomAccessFile#seek(long)}.
	 */
	public void seek(long pos) throws IOException {
		int n = (int)(real_pos - pos);
		if(n >= 0 && n <= buf_end) {
			buf_pos = buf_end - n;
		} else {
			super.seek(pos);
			invalidate();
		}
	}

	/**
	 * Used to replace the {@link RandomAccessFile#readLine()} method.
	 * This method first decides if the buffer still contains unread contents. If it doesn't, the buffer needs to be filled up.
	 * If the new line delimiter can be found in the buffer, then a new line is read from the buffer and converted into String.
	 * Otherwise, it will simply call the read method to read byte by byte.
	 * Although the code of the latter portion is similar to the original readLine, performance is better here because the read method is buffered in the new class.
	 * 
	 */
	 public final String getNextLine() throws IOException {
		String str = "";
		if(buf_end - buf_pos <= 0) {
			if(fillBuffer() <= 0) {
				//Nothing more to read
				return "";
			}
		}
		
		int lineend = -1;
		for(int i = buf_pos; i < buf_end; i++) {
			if(buffer[i] == '\n') {
				lineend = i;
				break;
			}
		}
		
		if(lineend < 0) {
			StringBuffer input = new StringBuffer(256);
			int c;
			while (((c = read()) != -1) && (c != '\n')) {
				input.append((char)c);
			}
			if ((c == -1) && (input.length() == 0)) {
				return "";
			}
			if(c == '\n')
				return input.toString();
			else
				return "";
		}
		
		if(lineend > 0 && buffer[lineend-1] == '\r') {
			str = new String(buffer, buf_pos, lineend - buf_pos -1, Charset.defaultCharset());
		} else {
			str = new String(buffer, buf_pos, lineend - buf_pos, Charset.defaultCharset());
		}
		buf_pos = lineend +1;
		
		return str;
	 }
}
