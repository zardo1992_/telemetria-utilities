package com.nttdata.admeter.output;

import java.io.File;
import java.io.IOException;
import org.apache.logging.log4j.Logger;
import com.nttdata.admeter.constant.ErrorCode;
import com.nttdata.admeter.constant.Parameter;
import com.nttdata.admeter.interfaces.IConfig;

/**
 * Offset element (a line) inside the offset file (related to one or more input files, in a single directory).
 * @author BurrafatoMa
 *
 */
public class FileOffsetElement {

	private File file;
	private	long size;
	private	long offset;
	private RandomAccessBufferedFile fileRandom;
	
	// Used to check if the offset was not updated
	private long previousOffset;
	private long lastTs;
	
	private Logger logger;
	private ErrorLogger errorLogger;
	private IConfig config;
	
	public FileOffsetElement(Logger logger, ErrorLogger errorLogger, IConfig config, File file, long size, long offset) {
		this.logger = logger;
		this.errorLogger = errorLogger;
		this.file = file;
		this.size = size;
		this.offset = offset;
		this.previousOffset = offset;
		this.config = config;
		this.lastTs = System.currentTimeMillis();
		fileRandom = null;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public long getOffset() {
		return offset;
	}

	public long getLastTs() {
		return lastTs;
	}
	
	public void setOffset(long offset) {
		this.offset = offset;
	}
	
	public long bytesToRead() {
		return size - offset;
	}	
		
	/**
	 * Reads the next buffer from the specified file. 
	 * @return 	the read buffer, without what follows the last newline; returns null if there is nothing to read.
	 */	
	public byte[] read() {
		byte[] result = null;
		try {
			if(logger.isDebugEnabled()) logger.debug("Reading file " + file.getName() + " current size: " + size + " current offset: " + offset);			
			// If file size is smaller than current offset, recycle by resetting the offset at start
			if (size < offset) { // Recycle
				offset = 0L;
			}

			previousOffset = offset;

			if (size > offset) {
				
				if(fileRandom == null){
					fileRandom = new RandomAccessBufferedFile(file, "r", config.getInteger(Parameter.BUFFER_SIZE), offset);
				}
				if(logger.isDebugEnabled()) logger.debug("Reading File " + file.getName() + " at position " + offset);
//				fileRandom.seek(offset);
				String message = fileRandom.getNextLine();
			    int readBytes = message.length();
			    
			    if (readBytes > 0) {
			    	offset = fileRandom.getFilePointer();
			    	if(logger.isDebugEnabled()) logger.debug("File " + file.getName() + " bytes read: " + readBytes + ", new offset: " + offset);
			    	
			    	if(size > 0 && offset > size){
			    		if(logger.isDebugEnabled()) logger.debug("Reading file " + file.getName() + " current size: " + size + " current offset: " + offset + ", too big, resetting");
			    		offset = size;
			    	}
			    	
			    	result = message.getBytes();
				    lastTs = System.currentTimeMillis();
			    } else {
			    	if(offset < size){
			    		if(logger.isDebugEnabled()) logger.debug("Reading file " + file.getName() + " current size: " + size + " current offset: " + offset + ", increasing");
			    		
			    		offset = fileRandom.getFilePointer();
			    	}
			    	if(logger.isDebugEnabled()) logger.debug("No line to read found in file = " + file.getAbsolutePath() + " at offset = " + offset);
			    }
			    
			}
		} catch (Exception e) {
			logger.error("Exception reading file " + file.getAbsolutePath() + "; ", e);
			errorLogger.write(ErrorCode.FILE_NOT_FOUND, "[module:Parser,method:FileOffsetElement.read,exception:"+e.getMessage()+"]");			
		}
		
		return result;	
	}
	
    /**
     * If you call read() you get a message and the offset encrease, call this method to put the message back to the
     * buffer.
     * @param message
     */
    public void putBackMessage(byte[] message) {
        this.offset -= message.length + 1; // +1 for \n char

        try {
            this.fileRandom.seek(offset);
        } catch (IOException e) {
            logger.error("Error moving back file pointer.", e);
        }

        // Used to check if the offset was not updated
        this.previousOffset = this.offset - 10; // -10 to move to a generic previousOffset
        this.lastTs = System.currentTimeMillis();
    }
	
	/**
	 * Gets the difference between the current offset and the previous offset.
	 * @return a value greater than 0 if the offset was updated in the last reading, 0 if it was not updated.
	 */
	public long lastOffsetUpdate(){
		return offset - previousOffset;
	}
	
	/**
	 * Get the file name.
	 * @return the file name.
	 */
	public String getFileName() {
		return file.getName();
	}
	
	/**
	 * Close the reference to the random access file.
	 * @return true if the close succeded, false otherwise.
	 */
	public boolean closeFile(){
		if (fileRandom != null) {
			try {
				fileRandom.close();
				fileRandom = null;
				return true;
			} catch (Exception e) {
				return false;
			}
		} else {
			return true;	
		}
	}
}
