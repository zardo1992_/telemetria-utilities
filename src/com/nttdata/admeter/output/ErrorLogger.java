package com.nttdata.admeter.output;

import java.io.File;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.nttdata.admeter.constant.ErrorCode;
import com.nttdata.admeter.constant.Output;
import com.nttdata.admeter.constant.Parameter;
import com.nttdata.admeter.constant.Statistics;
import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.interfaces.IConfigUpdate;
import com.nttdata.admeter.interfaces.IStatisticsMonitor;
import com.nttdata.admeter.interfaces.IStatusMonitor;
import com.nttdata.admeter.utils.JsonUtils;
import com.nttdata.admeter.utils.TimeUtils;

/**
 * Class used to log all the errors in a separate error log file.
 * @author BurrafatoMa
 * @version 1.0.0
 *
 */
public class ErrorLogger implements IConfigUpdate {

	/**
	 * Struct representing a single line in the ErrorDictionary json file.
	 *
	 */
	private class ErrorItem {
		String  errorSource;
		String  Severity;
		String  ErrorCode;
		String  Text;
	}
	
//	private final static String SEVERITY_NORMAL = "NORMAL";
	private final static String SEVERITY_WARNING = "WARNING";
	private final static String SEVERITY_ERROR = "ERROR";
	private final static String SEVERITY_CRITICAL = "CRITICAL";
	
	private IStatisticsMonitor statistics;
	
	private Logger logger = null;
	private IConfig config;
	private HashMap<String, ErrorItem> errorDictionary;
	private String errorSource;
	
	private FileBufferWriter fbWriter;
	
	/**
	 * Constructor for the error logger. Also loads the error dictionary definitions.
	 * @param status a reference to the status logger. Used for the status of the file buffer writer.
	 * @param statistics a reference to the statistics logger. used to increment the error counter.
	 * @param config a reference to the configuration container.
	 * @param logger a reference to the Log4j2 logger.
	 */
	public ErrorLogger(IStatusMonitor status, IStatisticsMonitor statistics, IConfig config, Logger logger) {
		this.statistics = statistics;
		this.config = config;
		this.logger = logger;
		this.errorSource = (config.getString(Parameter.SOURCE) != null) ?
				config.getString(Parameter.SOURCE) : ErrorCode.Source.COMMON;
		
		this.fbWriter = new FileBufferWriter(logger, this, config, status, statistics, 
				config.getString(Parameter.DIR_ERROR), 
				config.getString(Parameter.MODULE_NAME) + "-" + config.getString(Parameter.INSTANCE_NAME) + ".err", 
				false, 
				Parameter.ERROR_TS_FORMAT);
	
		update();
	}
	
	/**
	 * Stop the file buffer writer thread.
	 */
	public void stop(){
		if(fbWriter != null){
			fbWriter.terminate();
		}
	}
	
	/**
	 * Loads the error dictionary definitions.
	 * Also called when the error dictionary has changed in the file system.
	 */
	@Override
	public boolean update() {
		String filePath = config.getString(Parameter.ERROR_DICTIONARY_FILE_DIR) + config.getString(Parameter.ERROR_DICTIONARY_FILE_NAME);
		try {
			logger.info("Loading Error Dictionary...");

			JSONObject json = JsonUtils.getJson(filePath);
			
			if(errorDictionary == null)
				errorDictionary = new HashMap<String, ErrorItem>();
			
			if(json != null){
				
				int errorCount = 0;
				Iterator<?> keys = json.keys();
				while( keys.hasNext() ) {
				    String errorCode = (String) keys.next();
				    ErrorItem errorItem = new ErrorItem();
				    try{
					    JSONArray errorParams = (JSONArray) json.get(errorCode);
					    if (errorParams.length() != 3) {
					    	logger.warn("Skipping Error Dictionary incorrect entry:  " + errorCode);
							write(errorSource, ErrorCode.ERRDICT_INVALID,"[method:ErrorLogger.load,errorCode:" + errorCode + "]");
						} else{
							errorItem.errorSource = this.errorSource;
							errorItem.Severity = (String) errorParams.get(0);
							errorItem.ErrorCode = (String) errorParams.get(1);
							errorItem.Text = (String) errorParams.get(2);
							errorDictionary.put(errorCode, errorItem);
							errorCount++;
						}
				    }catch(JSONException e){}
				}
				logger.info("Error Dictionary loading done. Found " + errorCount + " error codes.");
				return true;
			} else {
				logger.warn("Error dictionary file is not found, or empty, or not a valid json. Not loading error configuration.");
				return false;
			}
		} catch (JSONException e) {
			logger.error("Exception parsing json file. Not loading error configuration. ", e);
			write(errorSource, ErrorCode.JSON_PARSE, "[method:ErrorLogger.load,file:" + filePath + ",message=" + e.getMessage() + "]");
			return false;
		}
	}
	
	public void write(String errorCode, String params){
		this.write(ErrorCode.Source.COMMON, errorCode, params);
	}
	
	/**
	 * Writes a new line in the error log file.
	 * @param errorCode the code of the error.
	 * @param params additional String parameters to be added after the error code, severity, and description.
	 */
	public void write(String source, String errorCode, String params){
		ErrorItem errorItem = null;
		try {	
			String currentTime = TimeUtils.long2String(this);
			
			if(errorDictionary != null){
				errorItem = errorDictionary.get(errorCode);
			}
			if (errorItem == null) {
				logger.warn("Error Dictionary code not found: " + errorCode + ". Using default");
				errorItem = new ErrorItem();
				errorItem.errorSource = source;
				errorItem.ErrorCode = ErrorCode.EXCEPTION_UNKNOWN;
				errorItem.Severity = SEVERITY_WARNING;
				errorItem.Text = "Error not available in dictionary";
			}else{
				errorItem.errorSource = source;
			}
			
			String line = 
					currentTime + Output.SEPARATOR + 
					errorItem.errorSource + Output.SEPARATOR +
					errorItem.Severity + Output.SEPARATOR + 
					errorItem.ErrorCode + Output.SEPARATOR + 
					errorItem.Text + Output.SEPARATOR + 
					params + "\n";
			
			fbWriter.write(line.getBytes(Charset.forName("UTF-8")));
			
		} catch (Exception e){
			logger.error("Exception writing error file: ", e);
		} finally {
			if(errorItem != null && (SEVERITY_ERROR.equalsIgnoreCase(errorItem.Severity) || SEVERITY_CRITICAL.equalsIgnoreCase(errorItem.Severity))){
				statistics.incrementInt(Statistics.GENERIC_ERRORS + source);	
			}
		}
	}
	
	
	/**
	 * Get error dictionary file name from configuration.
	 * @return the errorDictionary complete path.
	 */
	@Override
	public String getConfigFileName() {
		File file = new File(config.getString(Parameter.ERROR_DICTIONARY_FILE_DIR)+config.getString(Parameter.ERROR_DICTIONARY_FILE_NAME));
		return file.getName();
	}
}
