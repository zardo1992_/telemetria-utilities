package com.nttdata.admeter.output;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.Logger;
import org.json.JSONArray;

import com.nttdata.admeter.constant.ErrorCode;
import com.nttdata.admeter.constant.Output;
import com.nttdata.admeter.constant.Parameter;
import com.nttdata.admeter.constant.Statistics;
import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.runnable.StatRunnable;
import com.nttdata.admeter.runnable.StatusRunnable;

/**
 * Class that receives an offset file as input, and reads/writes its contents.
 * @author BurrafatoMa
 *
 */
public class FileOffsetReader {

	private Logger logger;
	private ErrorLogger errorLogger;
	private IConfig config;
	private StatRunnable statisticsRunnable;
	
	private File offsetFile; 
	private File inputFilesDir;
	private TreeMap<String, FileOffsetElement> fileOffsets;
	private Map.Entry<String, FileOffsetElement> currentOfsEntry;
	
	private Iterator<Map.Entry<String, FileOffsetElement>> foIterator;
	
	private int loadFactor = 0;
	
	// Used to check if the offset was not updated
	private long lastOffsetUpdate = 0;
	
	public synchronized long getLastOffsetUpdate(){
		return lastOffsetUpdate;
	}
	
	public TreeMap<String, FileOffsetElement> getFileOffsets() {
		return fileOffsets;
	}
	
	public FileOffsetReader(IConfig config, Logger logger, ErrorLogger errorLogger, StatRunnable statisticsRunnable) {
		this.config = config;
		this.logger = logger;
		this.errorLogger = errorLogger;
		this.statisticsRunnable = statisticsRunnable;
		this.inputFilesDir = new File(config.getString(Parameter.INPUT_FILE_DIR));
		this.offsetFile = new File(config.getString(Parameter.OFFSET_FILE_DIR), config.getString(Parameter.OFFSET_FILE_NAME));
		this.fileOffsets = new TreeMap<String, FileOffsetElement>();
		foIterator = null;
		loadOffsets();
	}

	/**
	 * Called at object creation. Reads the offset file, parses it, and stores all the references to to files in a treeMap.
	 */
	private void loadOffsets() {
		FileReader fReader = null;
		BufferedReader bReader = null;
		String line;
		String[] foel;
		
		if (offsetFile.exists()){
			logger.info("File offset " + offsetFile.getAbsolutePath() + " found with size " + offsetFile.length());
			try {
				fReader = new FileReader(offsetFile);
				bReader = new BufferedReader(fReader);
				// Read offset file and initialize entries
				while ((line = bReader.readLine()) != null) {
					foel = line.split(Output.SEPARATOR_REGEX);
					// If an entry is malformed, discard it
					if (foel.length == 2) {
						try {
							String fileName = foel[0];
							long offset = Long.parseLong(foel[1]);
							// If fileOffsets does not already contains some reference to the file, add it with an offset of 0
							if (!fileOffsets.containsKey(fileName)){
								File file = new File(inputFilesDir, fileName);
								fileOffsets.put(fileName, new FileOffsetElement(logger, errorLogger, config, file, 0L, offset));
								logger.debug("Loaded file " + fileName + " with offset " + offset + " and size " + file.length());
							}
						}catch (NumberFormatException e) {
							logger.error("Error reading offset file " + offsetFile.getAbsolutePath() + "; ", e);
							errorLogger.write(ErrorCode.FILE_NOT_FOUND, "[module:Parser,method:FileOffsetReader.loadOffsets,exception:"+e.getMessage()+"]");
						}
					}
				}	
			} catch (Exception e) {
				logger.error("Error reading offset file " + offsetFile.getAbsolutePath() + "; ", e);
				errorLogger.write(ErrorCode.FILE_NOT_FOUND, "[module:Parser,method:FileOffsetReader.loadOffsets,exception:"+e.getMessage()+"]");
			} finally {
				try{
					if (bReader != null)
						bReader.close();
					if (fReader != null)
						fReader.close();
				} catch(Exception e){
					logger.error("Error closing offset file reader for " + offsetFile.getAbsolutePath() + "; ", e);
					errorLogger.write(ErrorCode.FILE_NOT_FOUND, "[module:Parser,method:FileOffsetReader.loadOffsets,exception:"+e.getMessage()+"]");
				}
			}
		} else {
			logger.warn("File offset " + offsetFile.getAbsolutePath() + " was not found. Creating a new one...");
			try {
				offsetFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * Check the input directory and update the offsets treeMap.
	 * If the treeMap reference a file that has not been updated for more than a minute, close the reference.
	 * If the treeMap reference a file not existing anymore, close and delete the reference.
	 * If there is a file in the directory which is not present in the treeMap, add the reference.
	 * @return true if the treeMap has been correctly updated, false if the directory is not accessible or not present, or if an error occurred.
	 */
	public synchronized boolean syncFileOffsets() {
		boolean result = true;
		long time = System.currentTimeMillis();
		try {
			File[] dirFiles = inputFilesDir.listFiles();
			if (dirFiles == null) { 
				// Directory removed or otherwise inaccessible, no action
				result = false;
			} else {
				// Saving directory in hashMap
				Map<String, Long> fileMap = new HashMap<String, Long>();
				for (int i = 0; i < dirFiles.length; i++) {
					if(checkFile(dirFiles[i].getName()))
						fileMap.put(dirFiles[i].getName(), dirFiles[i].length());
				}
				// Check fileOffsets entries
				Iterator<Map.Entry<String, FileOffsetElement>> iterator = fileOffsets.entrySet().iterator();
				while(iterator.hasNext()) {
					Map.Entry<String, FileOffsetElement> foEntry = iterator.next();
					String fileName = foEntry.getKey();
					// Check fileOffset entries that have not been updated for more than a minute
					if( time - foEntry.getValue().getLastTs() > 60000){
						foEntry.getValue().closeFile();
					}
					
					// Delete from fileOffsets entries without an existing file
					if (!fileMap.containsKey(fileName)) {
						foEntry.getValue().closeFile();
						iterator.remove();
					}
				}
				// Add an entry in fileOffsets for each file, or update its size if already existing
				for (Map.Entry<String, Long> fileEntry : fileMap.entrySet()) {
					String fileName = fileEntry.getKey();
					Long size = fileEntry.getValue();
					FileOffsetElement foel = fileOffsets.get(fileName);
					if (foel == null) {
						foel = new FileOffsetElement(logger, errorLogger, config, new File(inputFilesDir, fileName), size, 0L);
						logger.debug("Loaded offset file " + fileName + " with offset " + 0 + " and size " + size);
					} else {
						foel.setSize(size);
					}
					fileOffsets.put(fileName, foel);	
				}
				result =  true;	
			}
		} catch (Exception e) {
			logger.error("Error syncing offset file " + offsetFile.getAbsolutePath() + "; ", e);
			errorLogger.write(ErrorCode.FILE_WRITE, "[module:Parser,method:FileOffsetReader.syncFileOffsets,exception:"+e.getMessage()+"]");
			result =  false;
		}
		return result;
	}
	
	/**
	 * Check if a file present in the input directory matches the criteria in the configuration file.
	 * if no "input file pattern" is found in config, we assume it is a "pass all" filter.
	 * Otherwise, we expect to find a pattern as a string, a regex, an array of strings, or an array of regexes.
	 * @param filename the file to check.
	 * @return true if the file name matches, false otherwise.
	 */
	private boolean checkFile(String filename){
		Object pattern = config.getObject(Parameter.INPUT_FILE_PATTERN);
		if(pattern == null){
			// No pattern parameter found: we assume it is a "pass all" filter
			return true;
		} else if(pattern instanceof String){
			return checkFilePattern(filename, (String) pattern);
		} else if(pattern instanceof JSONArray){
			for(int i = 0; i < ((JSONArray)pattern).length(); i++){
				if(checkFilePattern(filename, (String) ((JSONArray)pattern).get(i)))
					return true;
			}
			return false;
		} else {
			logger.error("Error reading input file pattern from config: returned object of type " + pattern.getClass().getSimpleName() + ": " + pattern.toString());
			return false;
		}
	}
	
	/**
	 * Check if a file present in the input directory matches the selection patterns in the configuration.
	 * @param filename the file to check.
	 * @param pattern the pattern that the file has to match.
	 * @return true if the file name matches, false otherwise.
	 */
	private boolean checkFilePattern(String filename, String pattern){
		// First, check if pattern is a normal filename, and if it is equal to filename
		if(filename != null && pattern != null && filename.contains(pattern)){
			return true;
		} else {
			// Else, check with regex
			try{
				Pattern regex = Pattern.compile(pattern);
				Matcher matcher = regex.matcher(filename);
				return matcher.find();
			} catch(Exception e){
				return false;
			}
		}
	}

	/**
	 * Overwrites the offset files with the values currently present in the treemap
	 * @return true if the file is correctly written, false otherwise.
	 */
	public synchronized boolean saveOffsets() {
		FileWriter fWriter = null;
		FileOffsetElement foel;
		logger.debug("Saving offsets...");
		try {
			fWriter = new FileWriter(offsetFile);
			// Loop over fileOffsets and write offset File
			for (Map.Entry<String, FileOffsetElement> foEntry : fileOffsets.entrySet()) {
				foel = foEntry.getValue();
				fWriter.write(foel.getFileName() + "|" + foel.getOffset() + "\n");
				if(logger.isDebugEnabled()) logger.debug("Saving offset: file = " + foel.getFileName() + " offset = " + foel.getOffset());
			}
			return true;		
		} catch (Exception e) {
			logger.error("Error updating offset file " + offsetFile.getAbsolutePath() + ";", e);
			errorLogger.write(ErrorCode.FILE_WRITE, "[module:Parser,method:FileOffsetReader.saveOffsets,exception:"+e.getMessage()+"]");
			return false;
		} finally {
			try {
				if (fWriter != null) 
					fWriter.close();
			} catch (Exception e) {
				logger.error("Error closing offset file writer " + offsetFile.getAbsolutePath() + ";", e);
				errorLogger.write(ErrorCode.FILE_WRITE, "[module:Parser,method:FileOffsetReader.saveOffsets,exception:"+e.getMessage()+"]");
			}
		}	
	}

	/**
	 * Reads a message from the buffer.
	 * @return a new message or null if there is nothing to read.
	 */
	public synchronized byte[] getNextMessage() {
		byte[] message = null;
		FileOffsetElement fileOffset;
		try {
			foIterator = fileOffsets.entrySet().iterator();
			
			while(message == null && foIterator.hasNext()){
				currentOfsEntry = foIterator.next();
				if (currentOfsEntry != null) {
					fileOffset = currentOfsEntry.getValue();
					if (fileOffset.bytesToRead() > 0) {
						logger.debug("Getting new message. File name: " + fileOffset.getFileName() + ", size: " + fileOffset.getSize() + " bytes to read: " + fileOffset.bytesToRead());
						message = fileOffset.read();
						if (message != null) {
							statisticsRunnable.incrementInt(Statistics.BUFFER_READ);
						}
						lastOffsetUpdate = fileOffset.lastOffsetUpdate();
					} else if(fileOffset.bytesToRead() < 0){ // Should never happen
						fileOffset.setOffset(fileOffset.getSize());
					}
				}
			}
			updateLoadFactor();
		} catch (Exception e) {
			logger.error("Error getting new buffer: ", e);
			errorLogger.write(ErrorCode.BUFFER_READ, "[module:Parser,method:FileOffsetReader.getNextBuffer,exception:"+e.getMessage()+"]");			
			foIterator = null;   // To solve concurrent modification. Next time get new iterator
		}
		
		return message;
	}
		
	/**
	 * Sleep some time between a input file reading and another. Update the status of the calling thread, in order to avoid false positives in run thread.
	 * @param statusRunnable the thread checking the status of all application's threads.
	 * @param index the thread's index to update every second.
	 * @throws InterruptedException 
	 */
	public void sleep(StatusRunnable statusRunnable, String index) throws InterruptedException {
		int sleepTime = (100 - loadFactor) * config.getInteger(Parameter.OFFSET_CYCLE_READ_INTERVAL) / 100;
//		logger.info("Thread " + Thread.currentThread().getName() + " sleeping for " + sleepTime + " milliseconds");
		for(int i = 0; i < sleepTime; i = i+1000){
		    Thread.sleep(Math.min(1000, sleepTime - i));
		    statusRunnable.setStatus(index, System.currentTimeMillis());
		}
	}
	
	/**
	 * Sleep some time between a input file reading and another. Update the status of the calling thread, in order to avoid false positives in run thread.
	 * @param status true if the module is actually OK and the timestamp has to be updated, false if the module is actually KO and it has to maintain it while sleeping.
	 * @param statusRunnable the thread checking the status of all application's threads.
	 * @param index the thread's index to update every second.
	 * @throws InterruptedException 
	 */
	public void sleep(boolean status, StatusRunnable statusRunnable, String index) throws InterruptedException {
		int sleepTime = (100 - loadFactor) * config.getInteger(Parameter.OFFSET_CYCLE_READ_INTERVAL) / 100;
//		logger.info("Thread " + Thread.currentThread().getName() + " sleeping for " + sleepTime + " milliseconds");
		
		for(int i = 0; i < sleepTime; i = i+1000){
		    Thread.sleep(Math.min(1000, sleepTime - i));
		    if(status)
		    	statusRunnable.setStatus(index, System.currentTimeMillis());
		    else
		    	statusRunnable.setStatus(index, -1);
		}
	}
	
    /**
     * Sleep some time if enable (0 otherwise), if sleeps it sleep ofsReadCycleIntervalMsec. Update the status of the
     * calling thread, in order to avoid false positives in run thread.
     * @param status true if the module is actually OK and the timestamp has to be updated, false if the module is
     *        actually KO and it has to maintain it while sleeping.
     * @param statusRunnable the thread checking the status of all application's threads.
     * @param index the thread's index to update every second.
     * @param enableSleep say if sllep or not
     * @throws InterruptedException
     */
    public void sleepCond(boolean status, StatusRunnable statusRunnable, String index, boolean enableSleep) throws InterruptedException {
        int sleepTime = enableSleep ? config.getInteger(Parameter.OFFSET_CYCLE_READ_INTERVAL) : 0;

        for (int i = 0; i < sleepTime; i = i + 1000) {
            Thread.sleep(Math.min(1000, sleepTime - i));
            if (status)
                statusRunnable.setStatus(index, System.currentTimeMillis());
            else
                statusRunnable.setStatus(index, -1);
        }
    }
	
	/**
	 * Updates the load factor of the module, based on the size and the offset of the input files.
	 * This will affect the sleep time during the thread's inactivity.
	 */
	public void updateLoadFactor(){
		long totalRead = 0;
		long totalSize = 0;
		for (Map.Entry<String, FileOffsetElement> foEntry : fileOffsets.entrySet()) {
			FileOffsetElement foel = foEntry.getValue();
			totalRead += foel.getOffset();
			totalSize += foel.getSize();
		}
		loadFactor = (int) ((totalSize > 0) ? 100 - ((double)totalRead / totalSize) * 100.0f : 0);
		if(logger.isDebugEnabled()) logger.debug("Updating load Factor: " + totalRead + "/" + totalSize + " = " + loadFactor);
		statisticsRunnable.updateLoadFactor(loadFactor);
	}
	
	public Logger getLogger() {
		return logger;
	}
	
	public StatRunnable getStatisticsRunnable() {
		return statisticsRunnable;
	}
	
	public ErrorLogger getErrorLogger() {
		return errorLogger;
	}
	
	public void setLastOffsetUpdate(long lastOffsetUpdate) {
		this.lastOffsetUpdate = lastOffsetUpdate;
	}
}
