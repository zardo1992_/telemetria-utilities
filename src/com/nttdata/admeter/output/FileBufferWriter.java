package com.nttdata.admeter.output;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.Logger;

import com.nttdata.admeter.constant.ErrorCode;
import com.nttdata.admeter.constant.Output;
import com.nttdata.admeter.constant.Parameter;
import com.nttdata.admeter.constant.Statistics;
import com.nttdata.admeter.constant.Status;
import com.nttdata.admeter.constant.StatusTimestamp;
import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.interfaces.IStatisticsMonitor;
import com.nttdata.admeter.interfaces.IStatusMonitor;

/**
 * Class to write the intermediate output files in the file system.
 * @author BurrafatoMa
 * @version 1.0.0
 *
 */
public class FileBufferWriter {
	
	private Logger logger;
	private ErrorLogger errorLogger;
	private IConfig config;
	private IStatusMonitor status;
	private IStatisticsMonitor statistics;
	private boolean shouldCountFlush = false;
	private String fileNameTsFormat;
	private String	errorSource;
	
	private	File	filePath;
	private	String	fileName;
	private	byte[]	buffer;
	private	int		offset;
	private Date	lastWrtTs;
	
	private Thread writerThread = null;
	
	/**
	 * Initialize the object and schedules an asynchronous writer.
	 * @param logger the Log4j2 logger.
	 * @param errorLogger the error logger.
	 * @param iConfig the Configuration object.
	 * @param status the Status runnable.
	 * @param statistics the Statistics runnable.
	 * @param filePath the absolute path of the output file.
	 * @param fileName the name of the output file.
	 * @param shouldCountFlush true if this writer flush should be counted in the statistics, false otherwise.
	 */
	public FileBufferWriter(Logger logger, ErrorLogger errorLogger, IConfig iConfig, IStatusMonitor status, IStatisticsMonitor statistics, String filePath, String fileName, boolean shouldCountFlush, String fileTsFormat) {
		this.logger = logger;
		this.errorLogger = errorLogger;
		this.config = iConfig;
		this.status = status;
		this.status.setStatus(Status.INDEX_BUFFERWRITER + fileName, new StatusTimestamp(System.currentTimeMillis(), config.getInteger(Parameter.BUFFER_REFRESH)));
		this.statistics = statistics;
		this.fileName = fileName;
		this.buffer = new byte[config.getInteger(Parameter.BUFFER_SIZE)];
		this.offset = 0;
		this.lastWrtTs = new Date();
		this.shouldCountFlush = shouldCountFlush;
		this.fileNameTsFormat = fileTsFormat;
		this.errorSource = (iConfig.getString(Parameter.SOURCE) != null) ?
				iConfig.getString(Parameter.SOURCE) : ErrorCode.Source.COMMON;
				
		this.filePath = new File(filePath);
		if (!this.filePath.exists()) 
			this.filePath.mkdirs();
		
		CheckAsyncWrite checkAsyncWrite = new CheckAsyncWrite();
		writerThread = new Thread(checkAsyncWrite, "fileWrtThr-"+nextBufferNum());
		writerThread.start();
	}
	
    /* For auto-numbering buffer writers. */
    private static int bufferInitNumber;
    private static synchronized int nextBufferNum() {
        return bufferInitNumber++;
    }
	
	/**
	 * Called when the servlet context is destroyed, to stop this thread after flushing the contents of the buffer.
	 */
	public void terminate() {
		flush();
		writerThread.interrupt();
    }
	
	/**
	 * Asynchronous writer thread.
	 * If after some time it has not written anything on file system, it flushes to the file the actual contents of the buffer.
	 */
	private class CheckAsyncWrite implements Runnable {
		public void run(){
			while (!writerThread.isInterrupted() && !Thread.currentThread().isInterrupted()) {
				try {
					status.setStatus(Status.INDEX_BUFFERWRITER + fileName, System.currentTimeMillis());
				    Thread.sleep((long) config.getInteger(Parameter.BUFFER_REFRESH));
				} catch(InterruptedException e) {
				    status.setStatus(Status.INDEX_BUFFERWRITER + fileName, -1L);
				    Thread.currentThread().interrupt();
				}						
				Date dt = new Date();
				long RunTime = (Long) (dt.getTime() - lastWrtTs.getTime());
				if (RunTime > (long) config.getInteger(Parameter.BUFFER_REFRESH)) {
					flush();
				}
			}	
		}
	}
	
	/**
	 * Writes on file system the current contents of the buffer.
	 */	
	public void flush() {
		
		synchronized (FileBufferWriter.class) {		
			// If there is nothing to write, do nothing
			if (offset > 0){
				FileOutputStream outFileStream = null;
				try {

					String outFileName = new SimpleDateFormat((String) config.getString(fileNameTsFormat)).format(new Date()) + fileName;
					File outFile = new File(this.filePath, outFileName);
					outFileStream = new FileOutputStream(outFile, true);
					outFileStream.write(buffer, 0, offset);
					// Allocate a new buffer: maybe its size have be changed from the config file
					if (buffer.length != config.getInteger(Parameter.BUFFER_SIZE)) {
						this.buffer = new byte[config.getInteger(Parameter.BUFFER_SIZE)];
					}
				
					if(shouldCountFlush){
						// We count flush only for output buffer writer, not for error log buffer writer
						logger.info("File flush executed (" + offset + " bytes)");
						statistics.incrementInt(Statistics.BUFFER_WRITE);
					}
					offset = 0;
					lastWrtTs = new Date();
				}
				catch (Exception e) {
					logger.error("Exception flushing:", e);
					errorLogger.write(errorSource, ErrorCode.FILE_WRITE, "[module:Utilities,method:FileBufferWriter.flush,exception:"+e.getMessage()+"]");	
					status.setStatus(Status.INDEX_BUFFERWRITER + fileName, -1L);
				} finally {
					try {
						if (outFileStream != null){
							outFileStream.flush();
							outFileStream.close();
						}
					} catch (Exception e) {
						logger.error("Exception closing flusher:", e);
						errorLogger.write(errorSource, ErrorCode.FILE_WRITE, "[module:Utilities,method:FileBufferWriter.flush,exception:"+e.getMessage()+"]");
					}				
				}
			}			
		}
	}
	
	/**
	 * Formats the values that will be added to the buffer.
	 * @param values an array of byte arrays containing the data to be written.
	 */
	public synchronized void writeByteArray(List<byte[]> values) {
		int  numberOfValues = 0;
		int  actualOfs;
		int  actualLen;
		byte[] vLen;
		byte[] vNum;
		byte[][] vOfs;
		String wrkStr;
		byte[] writeBuffer;
		int	writeBufferPos;

		if (values != null){
			// Number of data fields to be written
			numberOfValues = values.size();
			wrkStr = String.format("%03d%s", numberOfValues, Output.SEPARATOR);
			vNum = wrkStr.getBytes(Charset.forName("UTF-8"));
			// From the start of the buffer: length, number, offset...
			actualOfs = 9 + 4 + numberOfValues * 7;
			actualLen = vNum.length;
			
			// Allocate offsets
			vOfs = new byte[numberOfValues][];
			for (int ix = 0; ix < numberOfValues; ix++) {
				wrkStr = String.format("%06d%s",actualOfs,Output.SEPARATOR);
				vOfs[ix] = wrkStr.getBytes(Charset.forName("UTF-8"));				
				actualOfs += values.get(ix).length + 1;  // + separator
				actualLen += (values.get(ix).length + vOfs[ix].length);
				if (ix < (numberOfValues-1)) {
					actualLen += 1; // Separator for the values
				}
			}
			//Length of line
			wrkStr = String.format("%08d%s", actualLen + 9, Output.SEPARATOR);
			vLen = wrkStr.getBytes(Charset.forName("UTF-8"));
			actualLen += vLen.length;
			writeBuffer = new byte[actualLen+1];
			writeBufferPos = 0;
			// Write length + number of entries
			System.arraycopy(vLen, 0, writeBuffer, writeBufferPos, vLen.length);
			writeBufferPos += vLen.length;
			System.arraycopy(vNum, 0, writeBuffer, writeBufferPos, vNum.length);
			writeBufferPos += vNum.length;
			// Write offset
			for (int ix=0; ix < numberOfValues; ix++) {
				System.arraycopy(vOfs[ix], 0, writeBuffer, writeBufferPos, vOfs[ix].length);
				writeBufferPos += vOfs[ix].length;
			}
			// Write values
			byte[] sep = Output.SEPARATOR.getBytes(Charset.forName("UTF-8"));
			for (int ix=0; ix < numberOfValues; ix++) {
				System.arraycopy(values.get(ix), 0, writeBuffer, writeBufferPos, values.get(ix).length);
				writeBufferPos += values.get(ix).length;
				if (ix < (numberOfValues-1)) {
					System.arraycopy(sep, 0, writeBuffer, writeBufferPos, 1);
					writeBufferPos += 1;
				}
			}
			// Write final newline
			writeBuffer[writeBufferPos] = '\n';
			write(writeBuffer);
		}
	}

	/**
	 * Adds a line of data to the buffer, in order to be written on the intermediate output file.
	 * @param values a byte array containing the line to be written.
	 */
	public void write(byte[] values){
		int  freeSpace = 0;
		
		synchronized (FileBufferWriter.class) {
			freeSpace = buffer.length - offset;
			// Flush if free space is not enough for the current string to write
			if (freeSpace < values.length) {
				flush();
			}
			// Error if free space is not enough
			freeSpace = buffer.length - offset;
			if (freeSpace >= values.length) {
				System.arraycopy(values, 0, buffer, offset, values.length);
				offset += values.length;
				logger.debug("Buffer writing queued (" + values.length + " bytes)");
			} else {
				logger.error("Buffer writer size too small. Buffer size: " + freeSpace + " Requested size = " + values.length);
				errorLogger.write(errorSource, ErrorCode.BUFFER_SMALL, "[module:Utilities,bufferSize:" + freeSpace + ",requestedSize:" + values.length + "]");
			}			
			return;
		}
	}
	
	/**
	 * Get the file name.
	 * @return the file name.
	 */
	public String getFileName(){
		return fileName;
	}
}