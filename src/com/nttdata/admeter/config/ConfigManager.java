package com.nttdata.admeter.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Set;

import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.spi.json.JsonOrgJsonProvider;
import com.jayway.jsonpath.spi.mapper.JsonOrgMappingProvider;
import com.nttdata.admeter.interfaces.ConfigurationCounters;
import com.nttdata.admeter.interfaces.IConfig;

/**
 * Utility class to retrieve the app configuration from a json file.
 * 
 * @author BurrafatoMa
 * @version 1.0.0
 *
 */
public class ConfigManager implements IConfig {

	final private Configuration conf; // configurazioni del parser
	private DocumentContext ctx; // contesto in cui caricare il json
	private Logger logger;

	public ConfigManager() {
		conf = Configuration.builder().jsonProvider(new JsonOrgJsonProvider())
				.mappingProvider(new JsonOrgMappingProvider()).build();
		ctx = JsonPath.using(conf).parse("{}");
	}

	public ConfigManager(Logger logger) {
		this();
		this.logger = logger;
	}

	/**
	 * Loads the configuration from file.
	 * 
	 * @param filename
	 *            the path to the configuration file to load.
	 * @throws IOException
	 *             if the json file cannot be loaded or if it is not a valid
	 *             json object.
	 */
	public void load(final String filename) throws IOException {
		Throwable cause = null;
		for (int i = 0; i < 5; i++) {
			try {
				FileInputStream fileConfig = new FileInputStream(filename);
				ctx = JsonPath.using(conf).parse(fileConfig, "UTF-8");
				fileConfig.close();
			
				return;
				
			} catch (Exception e) {
				cause = e;
				try {
					Thread.sleep(500);
				} catch (InterruptedException ignore) {}
			}
		}
		
		if (cause != null)
			throw new IOException(cause.getMessage(), cause);
	}

	/**
	 * Gets a long parameter from the configuration.
	 * 
	 * @throws Exception
	 */
	public long getLong(final String key) {
		return new Long(getString(key));
	}

	/**
	 * Gets a float parameter from the configuration.
	 * 
	 * @throws Exception
	 */
	public float getFloat(final String key) {
		return Float.parseFloat(getString(key));
	}

	/**
	 * Checks if a parameter exists in the configuration.
	 * 
	 * @return true if the parameter exists, false otherwise.
	 */
	public boolean hasValue(final String key) {
		try {
			ctx.read(key);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return ctx.jsonString();
	}

	/**
	 * Gets a String parameter from the configuration.
	 * 
	 * @return the String value of the parameter or null
	 */
	@Override
	public String getString(String key) {
		String result = null;
		try {
			result = ctx.read(key, String.class).trim();
		} catch (Exception je) {
			logger.debug("getString(" + key + ") NOT FOUND");
		}
		if (logger != null)
			logger.trace("getString(" + key + ") = " + result);
		return result;
	}

	/**
	 * Gets a int parameter from the configuration.
	 * 
	 * @throws Exception
	 */
	@Override
	public int getInteger(String key) {
		return Integer.parseInt(getString(key));
	}

	/**
	 * Sets a String parameter in the configuration.
	 * 
	 * @throws Exception
	 */
	@Override
	public void setString(String key, String value) {
		if (logger != null)
			logger.trace("setString(" + key + " , " + value + ")");

		if (hasValue(key))
			ctx.set(key, value);
		else {
			provideParent(key);
			ctx.put(getParentPath(key), getLastName(key), value);
		}
	}

	/**
	 * Sets a int parameter in the configuration.
	 * 
	 * @throws Exception
	 */
	@Override
	public void setInt(String key, int value) {
		setString(key, String.valueOf(value));
	}

	/**
	 * Gets a JSONArray parameter from the configuration.
	 */
	@Override
	public JSONArray getArray(String key) {

		JSONArray result = null;
		try {
			result = ctx.read(key);
		} catch (Exception e) {
			if (logger != null)
				logger.debug("getArray(" + key + ") NOT FOUND");
		}
		return result;
	}

	@Override
	/**
	 * Set an array
	 * 
	 * @throws Exception
	 */
	public void setArray(String key, JSONArray value) {
		if (hasValue(key))
			ctx.set(key, value);
		else {
			provideParent(key);
			ctx.put(getParentPath(key), getLastName(key), value);
		}
	}

	/**
	 * Gets an Object parameter from the configuration.
	 */
	public Object getObject(String key) {
		Object result = null;
		try {
			result = ctx.read(key);
		} catch (Exception e) {
			if (logger != null)
				logger.debug("getObject(" + key + ") NOT FOUND");
		}
		return result;
	}

	@Override
	public JSONObject getJSONObject(String key) {

		JSONObject result = null;
		try {
			result = ctx.read(key);
		} catch (Exception e) {
			if (logger != null)
				logger.debug("getJSONObject(" + key + ") NOT FOUND");
		}
		return result;
	}

	/**
	 * Sets a long parameter in the configuration.
	 */
	@Override
	public void setLong(String key, long value) {
		setString(key, String.valueOf(value));
	}

	@Override
	public JSONObject asJson() {
		return ctx.json();
	}

	@Override
	public void loadParameters(IConfig config) {
		Set<String> chiavi = config.asJson().keySet();
		for (String key : chiavi) {
			ctx.put("$", key, config.asJson().get(key));
		}
		if (logger != null)
			logger.debug("loadParameters() :" + this.toString());
	}

	@Override
	public ConfigurationCounters load(String filename, String section) throws IOException {
		ConfigurationCounters cc = new ConfigurationCounters();
		try {
			ConfigManager cm = new ConfigManager();
			cm.load(filename);

			JSONObject aux = cm.getJSONObject(section);
			setObject(section, aux);
			System.out.println("[ConfigManager][load] Section: " + section + " new Conf: " + toString());

		} catch (IOException e) {
			e.printStackTrace();
			cc.error++;
		}
		return cc;
	}

	@Override
	public void remove(String key) {
	}

	@Override
	public void setObject(String key, JSONObject val) {
		if (hasValue(key))
			ctx.set(key, val);
		else {
			provideParent(key);
			ctx.put(getParentPath(key), getLastName(key), val);
		}
	}

	public static void main(String[] args) {

		ConfigManager cm = new ConfigManager();
		// cm.load("C:/usr/local/adMeter/config/ModuleCollector/adSmartDataCollector.json");

		System.out.println("prima: " + cm.toString());

		// cm.setString("$.A", "prova");
		// System.out.println("resp : " + cm.hasValue("$.b"));
		// cm.ctx.put("$", "e", new JSONObject());
		// cm.ctx.put("$.e", "f", new JSONArray());

		cm.setString("$.a.b[1].c", "ciao");

		System.out.println("dopo : " + cm.toString());
	}

	private static String getParentPath(String path) {

		int cut = path.lastIndexOf(".");
		if (cut != -1)
			return path.substring(0, cut);
		else
			return "$";
	}

	private static String getLastName(String path) {
		int cut = path.lastIndexOf(".");
		if (cut != -1)
			return path.substring(cut + 1, path.length());
		else
			return "";
	}

	private static boolean isArrayName(String name) {

		return name.contains("[");

	}

	/**
	 * Dato un persorso, fa si che il padre e tutti gli antenati siano presenti
	 * es: '$.fixed.config[2].name' se non esistenti crea oggetto fixed e un
	 * array config con 3 elementi
	 * 
	 * @param path
	 */
	private void provideParent(String path) {
		String parentPath = getParentPath(path);
		String parentName = getLastName(parentPath);

		if (hasValue(parentPath) == false) {

			if (isArrayName(parentName)) {
				String strNumber = parentName.substring(parentName.indexOf("[") + 1, parentName.indexOf("]"));
				String arrayPath = parentPath.substring(0, parentPath.indexOf("["));
				System.out.println("Numero: " + strNumber);
				int index = Integer.parseInt(strNumber);
				JSONArray jsa = new JSONArray();
				for (int i = 0; i <= index; i++)
					jsa.put(new JSONObject());
				System.out.println("jsa: " + jsa.toString());
				System.out.println("path: " + arrayPath);
				setArray(arrayPath, jsa);
			} else {
				setObject(parentPath, new JSONObject());
			}
		}

	}
}
