package com.nttdata.admeter.config.check;

import java.io.File;
import java.util.List;

import org.apache.logging.log4j.Logger;

import com.nttdata.admeter.interfaces.ConfigurationCounters;
import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.utils.FileUtils;

public class ConfigCheck {
	static final public String YES = "Y";
	static final public String NO = "N";
	
	protected final static String defaultDirStatus = File.separatorChar + "usr" + File.separatorChar + "local"
			+ File.separatorChar + "adMeter" + File.separatorChar + "status" + File.separatorChar + "run"
			+ File.separatorChar;
	protected final static String defaultDirStatistics = File.separatorChar + "usr" + File.separatorChar + "local"
			+ File.separatorChar + "adMeter" + File.separatorChar + "status" + File.separatorChar + "stat"
			+ File.separatorChar;
	protected final static String defaultDirError = File.separatorChar + "usr" + File.separatorChar + "local"
			+ File.separatorChar + "adMeter" + File.separatorChar + "status" + File.separatorChar + "error"
			+ File.separatorChar;
	protected final static String defaultDirOffset = File.separatorChar + "usr" + File.separatorChar + "local"
			+ File.separatorChar + "adMeter" + File.separatorChar + "output" + File.separatorChar + "ofs"
			+ File.separatorChar;
	protected final static String defaultDirOutput = File.separatorChar + "usr" + File.separatorChar + "local"
			+ File.separatorChar + "adMeter" + File.separatorChar + "output" + File.separatorChar;
	protected final static String defaultDirDatabase = File.separatorChar + "usr" + File.separatorChar + "local"
			+ File.separatorChar + "adMeter" + File.separatorChar + "database" + File.separatorChar;
	protected final static String defaultFileNameXMLSchema = "xmlSchema.xml";
	protected final static String defaultTimestampFilePrefix = "yyyyMMdd_";
	protected final static String defaultTimestampHFilePrefix = "yyyyMMdd_HH_";
	protected final static String defaultTimestampHSFilePrefix = "yyyyMMdd_HHmmss_SSS_";
	protected final static String defaultFileNameErrorDictionary = "errordictionary.json";
	protected final static String defaultFileNameTblChannel = "channeltable.json";
	protected final static String defaultHeaderID = "SUBSCRIBER";
	protected final static String defaultFileNameCSV = "file.csv";
	protected final static int defaultTsPosition = 2;
	protected final static int defaultKeyStoreRetryNum = 3;
	protected final static int defaultKeyStoreRetrySec = 1;

	/**
	 * Check the validity of a string parameter.
	 * 
	 * @param config
	 *            the configuration object.
	 * @param logger
	 *            the Log4j2 logger.
	 * @param parameter
	 *            the parameter to check.
	 * @param defaultValue
	 *            the default value to apply, if the current value is not valid.
	 * @param counter
	 *            the container of the correct/warning/error counters.
	 */
	protected static void checkStringDefault(IConfig config, Logger logger, String parameter, String defaultValue,
			ConfigurationCounters counter) {
		String value = config.getString(parameter);
		if (value == null) {
			logger.warn(
					"Parameter " + parameter + " not found in configuration. Setting default value: " + defaultValue);
			config.setString(parameter, defaultValue);
			counter.correct--;
			counter.warning++;
		} else if (value.length() == 0) {
			logger.warn("Parameter " + parameter + " has empty value in configuration. Setting default value: "
					+ defaultValue);
			config.setString(parameter, defaultValue);
			counter.correct--;
			counter.warning++;
		}
	}
	
	/**
	 * Check the validity of a string parameter inside a JSONObject.
	 * 
	 * @param config
	 *            the configuration object.
	 * @param logger
	 *            the Log4j2 logger.
	 * @param containingObj
	 * 			  the JSONObject where to search in.
	 * @param parameter
	 *            the parameter to check.
	 * @param defaultValue
	 *            the default value to apply, if the current value is not valid.
	 * @param counter
	 *            the container of the correct/warning/error counters.
	 
	protected static void checkStringDefault(IConfig config, Logger logger, JSONObject containingObj, String parameter, String defaultValue,
			ConfigurationCounters counter) {
		String value = containingObj.getString(parameter);
		if (value == null) {
			logger.warn(
					"Parameter " + parameter + " not found in configuration. Setting default value: " + defaultValue);
			containingObj.put(parameter, defaultValue);
			counter.correct--;
			counter.warning++;
		} else if (value.length() == 0) {
			logger.warn("Parameter " + parameter + " has empty value in configuration. Setting default value: "
					+ defaultValue);
			containingObj.put(parameter, defaultValue);
			counter.correct--;
			counter.warning++;
		}
	}
	*/
	/**
	 * Check the validity of a string parameter which refers to a mandatory directory/file
	 * (so there is no default value to set if the string is not valid).
	 * @param config
	 * 				the configuration object.
	 * @param logger
	 * 				the Log4j2 logger.
	 * @param parameter
	 * 				the parameter to check.
	 * @param counter
	 * 				the container of the correct/warning/error counters.
	 */
	protected static void checkStringNoDefault(IConfig config, Logger logger, String parameter, ConfigurationCounters counter){
		String value = config.getString(parameter);
		if (value == null) {
			logger.error("Parameter " + parameter + " not found in configuration. Program will not be able to start.");
			counter.correct--;
			counter.error++;
		} else if (value.length() == 0) {
			logger.error("Parameter " + parameter + " has empty value in configuration. Program will not be able to start.");
			counter.correct--;
			counter.error++;
		}
	}
	
	/**
	 * Check the validity of a string parameter which refers to a mandatory directory/file inside a JSONObject
	 * (so there is no default value to set if the string is not valid).
	 * @param config
	 * 				the configuration object.
	 * @param logger
	 * 				the Log4j2 logger.
	 * @param containingObj
	 * 				the JSONObject where to search in.
	 * @param parameter
	 * 				the parameter to check.
	 * @param counter
	 * 				the container of the correct/warning/error counters.
	 
	protected static void checkStringNoDefault(IConfig config, Logger logger, JSONObject containingObj, String parameter, ConfigurationCounters counter){
		String value = containingObj.getString(parameter);
		if (value == null) {
			logger.error("Parameter " + parameter + " not found in configuration. Program will not be able to start.");
			counter.correct--;
			counter.error++;
		} else if (value.length() == 0) {
			logger.error("Parameter " + parameter + " has empty value in configuration. Program will not be able to start.");
			counter.correct--;
			counter.error++;
		}
	}
	*/
	
	/**
	 * Check the validity of a string parameter which has to belong to a range of valid strings.
	 * @param config
	 * 				the configuration object.
	 * @param logger
	 * 				the Log4j2 logger.
	 * @param parameter
	 * 				the parameter to check.
	 * @param defaultValue
	 * 				the default value to apply, if the current value is not valid.
	 * @param counter
	 * 				the container of the correct/warning/error counters.
	 * @param validStrings
	 * 				the strings admitted.
	 */
	protected static void checkExpectedString(IConfig config, Logger logger, String parameter, String defaultValue,
			ConfigurationCounters counter, List<String> validStrings){
		String value = config.getString(parameter);
		boolean found = false;
		
		for (int i = 0; i < validStrings.size(); i++) {
			if (value.equals(validStrings.get(i))) {
				found = true;
			}
		}
		
		checkStringDefault(config, logger, parameter, defaultValue, counter);		
		if (!found) {
			logger.warn("Parameter " + parameter + " has not an expected value in configuration. Setting default value: "
					+ defaultValue);
			config.setString(parameter, defaultValue);
			counter.correct--;
			counter.warning++;
		}
	}

	/**
	 * Check the validity of a boolean parameter.
	 * 
	 * @param config
	 *            the configuration object.
	 * @param logger
	 *            the Log4j2 logger.
	 * @param parameter
	 *            the parameter to check.
	 * @param defaultValue
	 *            the default value to apply, if the current value is not valid.
	 * @param counter
	 *            the container of the correct/warning/error counters.
	 */
	protected static void checkBooleanDefault(IConfig config, Logger logger, String parameter, String defaultValue,
			ConfigurationCounters counter) {
		String value = config.getString(parameter);
		if (value == null) {
			logger.warn(
					"Parameter " + parameter + " not found in configuration. Setting default value: " + defaultValue);
			config.setString(parameter, defaultValue.toUpperCase());
			counter.correct--;
			counter.warning++;
		} else if (!YES.equalsIgnoreCase(value) && !NO.equalsIgnoreCase(value)) {
			logger.warn("Parameter " + parameter + " value is not valid (accepted values: \"Y\",\"N\",\"y\",\"n\"; value: \""
					+ value + "\"). Setting default value: " + defaultValue);
			config.setString(parameter, defaultValue.toUpperCase());
			counter.correct--;
			counter.warning++;
		}
	}
	
	
	/**
	 * Check the validity of a boolean parameter inside a JSONObject.
	 * 
	 * @param config
	 *            the configuration object.
	 * @param logger
	 *            the Log4j2 logger.
	 * @param containingObj
	 * 			  the JSONObject where to search in.
	 * @param parameter
	 *            the parameter to check.
	 * @param defaultValue
	 *            the default value to apply, if the current value is not valid.
	 * @param counter
	 *            the container of the correct/warning/error counters.
	 
	protected static void checkBooleanDefault(IConfig config, Logger logger, JSONObject containingObj, String parameter, String defaultValue,
			ConfigurationCounters counter) {
		String value = containingObj.getString(parameter);
		if (value == null) {
			logger.warn(
					"Parameter " + parameter + " not found in configuration. Setting default value: " + defaultValue);
			containingObj.put(parameter, defaultValue);
			counter.correct--;
			counter.warning++;
		} else if (!YES.equalsIgnoreCase(value) && !NO.equalsIgnoreCase(value)) {
			logger.warn("Parameter " + parameter + " value is not valid (accepted values: \"Y\",\"N\"; value: \""
					+ value + "\"). Setting default value: " + defaultValue);
			containingObj.put(parameter, defaultValue);
			counter.correct--;
			counter.warning++;
		}
		
		// I convert the y/n to uppercase
		config.setString(parameter, config.getString(parameter).toUpperCase());
	}
	 */
	
	/**
	 * Check the validity of an integer parameter.
	 * 
	 * @param config
	 *            the configuration object.
	 * @param logger
	 *            the Log4j2 logger.
	 * @param parameter
	 *            the parameter to check.
	 * @param defaultValue
	 *            the default value to apply, if the current value is not valid.
	 * @param multiplier
	 *            the multiplier to apply to the value to set, according to its
	 *            measurement unit (e.g. if we have to convert seconds to
	 *            milliseconds, the multiplier will be 1000).
	 * @param counter
	 *            the container of the correct/warning/error counters.
	 */
	protected static void checkIntDefault(IConfig config, Logger logger, String parameter, int defaultValue,
			int multiplier, ConfigurationCounters counter) {
		String value = config.getString(parameter);
		if (value == null) {
			logger.warn(
					"Parameter " + parameter + " not found in configuration. Setting default value: " + defaultValue );
			config.setInt(parameter, defaultValue * multiplier);
			counter.correct--;
			counter.warning++;
		} else {
			try {
				int valueBase = Integer.parseInt(value);
				if (valueBase <= 0) {
					logger.warn(
							"Parameter " + parameter + " found with negative value in configuration. Setting default value: " + defaultValue );
					config.setInt(parameter, defaultValue * multiplier);
					counter.correct--;
					counter.warning++;
				} else {
					config.setInt(parameter, valueBase * multiplier);
				}
			} catch (NumberFormatException e) {
				logger.warn("Parameter " + parameter + " value cannot be converted to int (value: \"" + value
						+ "\"). Setting default value: " + defaultValue);
				config.setInt(parameter, defaultValue * multiplier);
				counter.correct--;
				counter.warning++;
			}
		}
	}

	/**
	 * Check the validity of an integer parameter. It also checks it is a
	 * divisor of 60 (in order to assure a homogeneous granularity in a period
	 * of an hour).
	 * 
	 * @param config
	 *            the configuration object.
	 * @param logger
	 *            the Log4j2 logger.
	 * @param parameter
	 *            the parameter to check.
	 * @param defaultValue
	 *            the default value to apply, if the current value is not valid.
	 * @param multiplier
	 *            the multiplier to apply to the value to set, according to its
	 *            measurement unit (e.g. if we have to convert seconds to
	 *            milliseconds, the multiplier will be 1000).
	 * @param counter
	 *            the container of the correct/warning/error counters.
	 */
	protected static void checkIntMinuteDefault(IConfig config, Logger logger, String parameter, int defaultValue,
			int multiplier, ConfigurationCounters counter) {
		String value = config.getString(parameter);
		if (value == null) {
			logger.warn(
					"Parameter " + parameter + " not found in configuration. Setting default value: " + defaultValue);
			config.setInt(parameter, defaultValue);
			counter.correct--;
			counter.warning++;
		} else {
			try {
				int valueBase = Integer.parseInt(value);
				if (60 % valueBase != 0) {
					logger.warn("Parameter " + parameter + " is not a divisor of 60 (value:" + value
							+ "). This will not assure a homogeneous granularity in a period of an hour. Setting default value: "
							+ defaultValue + " msec");
					config.setInt(parameter, defaultValue);
					counter.correct--;
					counter.warning++;
				} else {
					config.setInt(parameter, valueBase * multiplier);
				}
			} catch (NumberFormatException e) {
				logger.warn("Parameter " + parameter + " value cannot be converted to int (value: \"" + value
						+ "\"). Setting default value: " + defaultValue);
				config.setInt(parameter, defaultValue);
				counter.correct--;
				counter.warning++;
			}
		}
	}

	/**
	 * Check the validity of an long parameter.
	 * 
	 * @param config
	 *            the configuration object.
	 * @param logger
	 *            the Log4j2 logger.
	 * @param parameter
	 *            the parameter to check.
	 * @param defaultValue
	 *            the default value to apply, if the current value is not valid.
	 * @param multiplier
	 *            the multiplier to apply to the value to set, according to its
	 *            measurement unit (e.g. if we have to convert seconds to
	 *            milliseconds, the multiplier will be 1000).
	 * @param counter
	 *            the container of the correct/warning/error counters.
	 */
	protected static void checkLongDefault(IConfig config, Logger logger, String parameter, long defaultValue,
			int multiplier, ConfigurationCounters counter) {
		String value = config.getString(parameter);
		if (value == null) {
			logger.warn(
					"Parameter " + parameter + " not found in configuration. Setting default value: " + defaultValue);
			config.setLong(parameter, defaultValue);
			counter.correct--;
			counter.warning++;
		} else {
			try {
				long valueBase = Long.parseLong(value);
				config.setLong(parameter, valueBase * multiplier);
			} catch (NumberFormatException e) {
				logger.warn("Parameter " + parameter + " value cannot be converted to long (value: \"" + value
						+ "\"). Setting default value: " + defaultValue);
				config.setLong(parameter, defaultValue);
				counter.correct--;
				counter.warning++;
			}
		}
	}
	
	protected static void checkDirExist(IConfig config, String parameter, Logger logger) {
		config.setString(parameter, FileUtils.checkDirectory(config.getString(parameter), logger));
	}
}
