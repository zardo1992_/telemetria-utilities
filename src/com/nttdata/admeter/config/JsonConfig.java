package com.nttdata.admeter.config;

import java.io.IOException;
import java.util.Iterator;

import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.nttdata.admeter.constant.Parameter;
import com.nttdata.admeter.interfaces.ConfigurationCounters;
import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.utils.JsonUtils;

/**
 * Utility class to retrieve the app configuration from a json file.
 * @author BurrafatoMa
 * @version 1.0.0
 *
 */
public class JsonConfig implements IConfig /*, IConfigUpdate*/ {

	private	JSONObject	jsonProps;
	private Logger logger;
	
	public JsonConfig(Logger logger){
		jsonProps = new JSONObject();
		this.logger = logger;
	}
	
	public JsonConfig(final String filename, Logger logger) throws IOException{
		this(logger);
		loadJson(filename, Parameter.SECTION_FIXED);
	}
	
	/**
	 * Loads the configuration from file.
	 * @param filename the path to the configuration file to load.
	 * @param section the section to load (fixed or runtime). If this parameter is null, both are loaded.
	 * @return the counters of the parameters loaded (number of correct loads, number of warnings, number of errors).
	 * @throws IOException if the json file cannot be loaded or if it is not a valid json object.
	 */
	public ConfigurationCounters load(final String filename, String section) throws IOException {
		return loadJson(filename, section);
	}
	
	/**
	 * Loads the specified section of a json file.
	 * @param filename the path to the configuration file to load.
	 * @param section the section to load (fixed or runtime). If this parameter is null, both are loaded.
	 * @return the counters of the parameters loaded (number of correct loads, number of warnings, number of errors).
	 * @throws IOException if the json file cannot be loaded or if it is not a valid json object.
	 */
	public ConfigurationCounters loadJson(final String filename, String section) throws IOException {
		JSONObject json = JsonUtils.getJson(filename);
		ConfigurationCounters result = new ConfigurationCounters();
		if(json != null){
			if(section != null){
				extractKeys(JsonUtils.getJsonObject(json, section), result);
				if(result.error > 0)
					logger.error("Could not load section "+section+" from json: section is null or not found or invalid.");
			} else {
				extractKeys(JsonUtils.getJsonObject(json, Parameter.SECTION_FIXED), result);
				if(result.error > 0)
					logger.error("Could not load section "+Parameter.SECTION_FIXED+" from json: section is null or not found or invalid.");
				extractKeys(JsonUtils.getJsonObject(json, Parameter.SECTION_RUNTIME), result);
				if(result.error > 0)
					logger.error("Could not load section "+Parameter.SECTION_RUNTIME+" from json: section is null or not found or invalid.");
			}
		} else {
			result.error++;
			logger.error("Could not load JSON config. Please check that config file " + filename + " is present and is a correct JSON");
		}
		return result;
	}
	
	/**
	 * Parses a json and stores it in the servlet context properties.
	 * @param json the json object to parse.
	 * @param result the counters of the results (number of correct loads, number of warnings, number of errors).
	 */
	private void extractKeys(JSONObject json, ConfigurationCounters result){
		
		if(json != null){
			Iterator<?> keys = json.keys();
			while( keys.hasNext() ) {
			    String key = (String)keys.next();
			    
			    Object value = json.get(key);
			    if(value instanceof String){
					
				    jsonProps.put(key, json.getString(key));
				    result.correct++;
				    if(logger.isDebugEnabled()) logger.debug("Loaded key " + key + " string " + json.get(key));
			   
			    } else if(value instanceof Integer){
			    
			    	jsonProps.put(key, String.valueOf(json.getInt(key)));
			    	result.correct++;
			    	if(logger.isDebugEnabled()) logger.debug("Loaded key " + key + " integer " + json.get(key));
			    
			    } else if(value instanceof JSONObject){
			    	
			    	jsonProps.put(key, value);
			    	result.correct += ((JSONObject) value).length();
			    	if(logger.isDebugEnabled()) logger.debug("Loaded key " + key + ", object containing " + ((JSONObject) value).length() + " parameters: " + ((JSONObject) value).toString());
			    	
			    } else if(value instanceof JSONArray){
			    	
			    	jsonProps.put(key, value);
			    	result.correct += ((JSONArray) value).length();
			    	if(logger.isDebugEnabled()) logger.debug("Loaded key " + key + " array of length " + ((JSONArray)value).length() + ": " + ((JSONArray) value).toString());
			    
			    }
			}
		} else {
			result.error++;
		}
	}

	/**
	 * Gets a byte parameter from the configuration.
	 * @param key the parameter to get.
	 * @return the byte value of the parameter.
	 
	public Byte getByte(final String key) {
		Byte result = null;
		try {
			byte b = Byte.parseByte(getString(key));
			result = new Byte(b);
		} catch (NumberFormatException nfe) {
			result = null;
		}
		return result;
	}
	 */
	
	/**
	 * Gets a long parameter from the configuration.
	 * @param key the parameter to get.
	 * @return the long value of the parameter.
	 */
	public long getLong(final String key) {
		long result;
		try {
			Long.parseLong(getString(key));
			result = new Long(getString(key));
		} catch (NumberFormatException nfe) {
			result = -1;
		}
		return result;
	}

	/**
	 * Gets a float parameter from the configuration.
	 * @param key the parameter to get.
	 * @return the float value of the parameter.
	 */
	public float getFloat(final String key) {
		float result = -1;
		try {
			result = Float.parseFloat(getString(key));
		} catch (NumberFormatException nfe) {
			result = -1;
		}
		return result;
	}

	public JSONObject asJson() {
		return jsonProps;
	}
	
	/**
	 * Checks if a parameter exists in the configuration.
	 * @param key the value to check.
	 * @return true if the parameter exists, false otherwise.
	 */
	public boolean hasValue(final String key) {
		if (jsonProps.has(key)) {
			return (jsonProps.get(key) != null && !jsonProps.get(
					key).equals(""));
		}
		return false;
	}


	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		jsonProps = null;
	}

	@Override
	public String toString() {
		return jsonProps.toString();
	}

	/**
	 * Gets a String parameter from the configuration.
	 * @param key the parameter to get.
	 * @return the String value of the parameter.
	 */
	@Override
	public String getString(String key) {
		String result = null;		
		try {
			result = (String) jsonProps.get(key);
		} catch (JSONException je) {
			result = "";
		}
		return result;
	}

	/**
	 * Gets a int parameter from the configuration.
	 * @param key the parameter to get.
	 * @return the int value of the parameter.
	 */
	@Override
	public int getInteger(String key) {
		int result = -1;
		try {
			result = Integer.parseInt(getString(key));
		} catch (NumberFormatException nfe) {
			result = -1;
		}
		return result;
	}

	/**
	 * Sets a String parameter in the configuration.
	 * @param key the parameter to set.
	 * @param value the String value of the parameter.
	 */
	@Override
	public void setString(String key, String value) {
		jsonProps.put(key, value);
	}
	
	/**
	 * Sets a int parameter in the configuration.
	 * @param key the parameter to set.
	 * @param value the int value of the parameter.
	 */
	@Override
	public void setInt(String key, int value) {
		setString(key, String.valueOf(value));
	}
	
	/**
	 * Sets a JSONObkect parameter in the configuration.
	 * @param key the parameter to set.
	 * @param value the JSONObject value of the parameter.
	 */
	@Override
	public void setObject(String key, JSONObject value) {
		jsonProps.put(key, value);
	}

	/**
	 * Sets a JSONArray parameter in the configuration.
	 * @param key the parameter to set.
	 * @param value the JSONArray value of the parameter.
	 */
	@Override
	public void setArray(String key, JSONArray value) {
		jsonProps.put(key, value);
	}
	
	/**
	 * Gets a JSONArray parameter from the configuration.
	 * @param key the parameter to get.
	 * @return the JSONArray value of the parameter.
	 */
	@Override
	public JSONArray getArray(String key){
		try{
			return jsonProps.getJSONArray(key);
		} catch(JSONException e){
			return null;
		}
	}

	/**
	 * Gets a JSONArray parameter's element from the configuration.
	 * @param key the parameter to get.
	 * @param itemNumber the number of the item of the JSONArray to retrieve.
	 * @return the JSONArray's element value.
	 */
	public JSONObject getArray(String key, int itemNumber) {
		try{
			return getArray(key).getJSONObject(itemNumber);
		} catch(JSONException e){
			return null;
		}
	}

	/**
	 * Gets an Object parameter from the configuration.
	 * @param key the parameter to get.
	 * @return the Object value of the parameter.
	 */
	public Object getObject(String key) {
		try{
			return jsonProps.get(key);
		} catch(JSONException e){
			return null;
		}
	}

	/**
	 * Removes a parameter from the configuration.
	 * @param key the parameter to remove.
	 */
	@Override
	public void remove(String key) {
		jsonProps.remove(key);
		
	}
	
	
	/**
	 * Gets a JSONObject parameter from the configuration.
	 * @param key the parameter to get.
	 * @return the JSONObject value of the parameter.
	 */
	@Override
	public JSONObject getJSONObject(String key) {
		try{
			return jsonProps.getJSONObject(key);
		} catch(JSONException e){
			return null;
		}
	}
	
	/**
	 * Loads a IConfig object's content in this current configuration object.
	 * @param config the object to load.
	 */
	@Override
	public void loadParameters(IConfig config) {
		Iterator<String> it = config.asJson().keys();
		
		while(it.hasNext()){
			String key = it.next();
			jsonProps.put(key, config.getObject(key));
		}
	}

	/**
	 * Sets a long parameter in the configuration.
	 * @param key the parameter to set.
	 * @param value the long value of the parameter.
	 */
	@Override
	public void setLong(String key, long value) {
		setString(key, String.valueOf(value));		
	}
}
