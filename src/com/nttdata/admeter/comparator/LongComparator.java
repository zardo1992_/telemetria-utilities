package com.nttdata.admeter.comparator;

import java.util.Comparator;

public class LongComparator implements Comparator<Long> {

	@Override
	public int compare(Long o1, Long o2) {
		// TODO Auto-generated method stub
		return Long.compare(o1, o2);
	}
}
