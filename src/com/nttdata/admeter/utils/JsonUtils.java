package com.nttdata.admeter.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

public class JsonUtils {
	
	/**
	 * Get a JSONObject from a text file containing a json.
	 * @param filename the file to read.
	 * @return the JSONObject extracted from the file, or null in case of errors.
	 */
	public static JSONObject getJson(String filename) {
		JSONObject jsonObject = null;
		BufferedReader input = null;
		FileReader inputFile = null;
		StringBuilder text = new StringBuilder();
		File file = new File(filename);
		if(file.exists()){
		    try{
		    	inputFile = new FileReader(file);
				input = new BufferedReader(inputFile);
			    String line;
			    while ((line = input.readLine()) != null) {
			        text.append(line);
			    }
			    jsonObject = new JSONObject(text.toString());
		    } catch(IOException | JSONException e){
		    	jsonObject = null;
		    } finally {
		    	try {
		    		if(input != null)
		    			input.close();
		    		if(inputFile != null)
		    			inputFile.close();
				} catch (IOException e) {}
		    }
	    }
	    return jsonObject;
	}
	
	/**
	 * Checks if a String is a valid json or not.
	 * @param test the String to test.
	 * @param logger the Log4j2 logger.
	 * @return true if it is a valid json, false otherwise.
	 */
	public static JSONObject isValidJson(String test, Logger logger) {
		JSONObject result = null;
		if(test == null || test.length() == 0){
			logger.warn("Json string is null or empty");
			return null;
		}
		
	    try {
	    	result = new JSONObject(test);
	    } catch (JSONException e) {
	    	logger.warn("Malformed json string: " + e.getMessage());
            return null;
	    }
	    return result;
	}

	/**
	 * Tries to fix a String that is not a valid json.
	 * @param broken the String to fix.
	 * @param logger the Log4j2 logger.
	 * @return the fixed String, if it was possible to fix, null otherwise.
	 */
	public static JSONObject fixJson(String broken, Logger logger) {
		try {
			org.json.simple.parser.JSONParser parser = new JSONParser();
			org.json.simple.JSONObject o2 = (org.json.simple.JSONObject) parser.parse(broken);
			if(logger.isInfoEnabled()) logger.info("Json string fixed: " + o2.toJSONString());
			return new JSONObject(o2.toJSONString());
		} catch (org.json.simple.parser.ParseException pe) {
			logger.warn("Malformed json string: " + pe.getMessage() + ", position " + pe.getPosition());
			return null;
		}		
	}
	
	/**
	 * Gets a String from a json.
	 * @param json the json where to search.
	 * @param key the key of the String to search.
	 * @return the String value if the key was found, an empty String otherwise.
	 */
	public static String getString(JSONObject json, String key){
		try{
			return json.getString(key);
		} catch(JSONException e){
			return "";
		}
	}
	
	/**
	 * Gets a String from a json array.
	 * @param json the json where to search.
	 * @param index the index in the array the String to search.
	 * @return the String value if the index was found, an empty String otherwise.
	 */
	public static String getString(JSONArray json, int index){
		try{
			return json.getString(index);
		} catch(JSONException e){
			return "";
		}
	}
	
	/**
	 * Gets a JSONObject from a json.
	 * @param json the json where to search.
	 * @param key the key of the json to search.
	 * @return the JSONObject value if the key was found, null otherwise.
	 */
	public static JSONObject getJsonObject(JSONObject json, String key){
		try{
			return json.getJSONObject(key);
		} catch(JSONException e){
			return null;
		}
	}
	
	/**
	 * Gets a JSONObject from a json array.
	 * @param json the json where to search.
	 * @param index the index in the array the json to search.
	 * @return the JSONObject value if the key was found, null otherwise.
	 */
	public static JSONObject getJsonObject(JSONArray json, int index){
		try{
			return json.getJSONObject(index);
		} catch(JSONException e){
			return null;
		}
	}
	
	//TODO: metodo per printare un json su file
}
