package com.nttdata.admeter.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import com.nttdata.admeter.constant.ErrorCode;
import com.nttdata.admeter.constant.Output;
import com.nttdata.admeter.output.ErrorLogger;

public class TimeUtils {
	
	/**
	 * Converts a timestamp in epoch time to a human readable String (with granularity of seconds).
	 * @param time the timestamp in epoch time.
	 * @param errorLogger the logger for possible errors.
	 * @return the converted String if time is greater than 0, "NA" otherwise.
	 */
	public static String long2String(long time, ErrorLogger errorLogger){
		String result = "NA";
		if(time > 0){
			try{
				result = new SimpleDateFormat(Output.TIMESTAMP_LOCAL_FORMAT).format(new Date(time));
			} catch (Exception e) {
				errorLogger.write(ErrorCode.TS_FORMAT_INVALID, "[method:long2string,time:"+time+",format:"+Output.TIMESTAMP_FORMAT+",exception:"+e.getMessage()+"]");
			}			
		}
		return result;
	}
	
	/**
	 * Converts the current machine timestamp in epoch time to a human readable String (with granularity of milliseconds).
	 * @param errorLogger the logger for possible errors.
	 * @return the converted String in human readable form.
	 */
	public static String long2String(ErrorLogger errorLogger){
		String result = "";
		try{
			result = new SimpleDateFormat(Output.TIMESTAMP_FORMAT).format(new Date());
		} catch (Exception e) {
			errorLogger.write(ErrorCode.TS_FORMAT_INVALID, "[method:long2string,time:now,format:"+Output.TIMESTAMP_FORMAT+",exception:"+e.getMessage()+"]");
		}
		return result;
	}
	
	/**
	 * Converts a human readable String to a timestamp in epoch time.
	 * @param time the timestamp in a human readable form.
	 * @param errorLogger the logger for possible errors.
	 * @return the converted long if the String is in the correct format, -1 otherwise.
	 */
	public static long string2long(String time, ErrorLogger errorLogger){
		long result = -1;
		try {			
			result = new SimpleDateFormat(Output.TIMESTAMP_FORMAT).parse(time).getTime();
		} catch (Exception e) {
			errorLogger.write(ErrorCode.TS_FORMAT_INVALID, "[method:string2long,time:"+time+",format:"+Output.TIMESTAMP_FORMAT+",exception:"+e.getMessage()+"]");
		}
		return result;
	}
	
	/**
	 * Converts a human readable String in UTC format to a timestamp in epoch time.
	 * @param time the timestamp in a human readable form.
	 * @param errorLogger the logger for possible errors.
	 * @return the converted long if the String is in the correct format, -1 otherwise.
	 */
	
	public static long UTCstring2long(String time, ErrorLogger errorLogger){
		long result = -1;
		try {			
			result = new SimpleDateFormat(Output.TIMESTAMP_UTC_FORMAT).parse(time).getTime();
		} catch (Exception e) {
			errorLogger.write(ErrorCode.TS_FORMAT_INVALID, "[method:string2long,time:"+time+",format:"+Output.TIMESTAMP_FORMAT+",exception:"+e.getMessage()+"]");
		}
		return result;
	}
	
	/**
	 * Converts a UTC timestamp in a local timestamp.
	 * @param UTCTime timestamp to convert
	 * @param timeZone
	 * @param errorLogger
	 * @return converted timestamp
	 */
	
	public static String UTC2LocalTime(String UTCTime, String timeZone, ErrorLogger errorLogger){
		String localTime = "";
		
		DateFormat utcFormat = new SimpleDateFormat(Output.TIMESTAMP_UTC_FORMAT);
		utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		Date date = null;
		try {
			date = utcFormat.parse(UTCTime);
			DateFormat localFormat = new SimpleDateFormat(Output.TIMESTAMP_LOCAL_FORMAT);
			localFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
			localTime = localFormat.format(date);
			
		} catch (ParseException e) {
			errorLogger.write(ErrorCode.TS_FORMAT_INVALID, "[method:UTC2LocalTime,time:"+UTCTime+",format:"+Output.TIMESTAMP_UTC_FORMAT+",exception:"+e.getMessage()+"]");
		}
		
		return localTime;
	}

	/**
	 * Converts a timestamp in epoch time to a human readable String (with granularity of minutes).
	 * @param time the timestamp in epoch time.
	 * @param errorLogger the Log4j2 logger for possible errors.
	 * @return the converted String.
	 */
	public static String long2minutes(long time, ErrorLogger errorLogger, String tsFormat) {
		String result = "";
		try{
			result = new SimpleDateFormat(tsFormat).format(new Date(time));
		} catch (Exception e) {
			errorLogger.write(ErrorCode.TS_FORMAT_INVALID, "[method:long2string,time:"+time+",format:"+tsFormat+",exception:"+e.getMessage()+"]");
		}
		return result;
	}
	
	/**
	 * Receives a timestamp with milliseconds, and truncates seconds and milliseconds.
	 * @param timestamp the initial epoch timestamp.
	 * @return the truncated epoch timestamp.
	 */
	
	public static long truncateToMinute(long timestamp){
		Date d = new Date(timestamp);
		Calendar c = new GregorianCalendar();
        c.setTime(d);

        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        
        return c.getTime().getTime();
	}
}
