package com.nttdata.admeter.utils;

import java.io.File;

import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import com.nttdata.admeter.constant.Parameter;
import com.nttdata.admeter.interfaces.ConfigurationCounters;
import com.nttdata.admeter.interfaces.IConfig;

public class FileUtils {

	
	
	/**
	 * Checks if a directory ends with file separator, and in case, adds it;
	 * Checks if a directory exists in the file system, and in case, creates it.
	 * @param dir the directory to check.
	 * @param logger the Log4j2 logger.
	 * @return the directory checked.
	 */
	public static String checkDirectory(String dir, Logger logger) {
	   	if(!dir.endsWith(String.valueOf("/"))){
				dir += "/";
		}
					
		File file = new File(dir);
		if(!file.exists()){
			logger.warn("Directory " + dir + " did not exist, creating...");
			file.mkdirs();
		}
		return dir;
	}
	
	public static String checkFileMandatory(String path, Logger logger, ConfigurationCounters counter){
		if (path.startsWith("~"))
			path = path.replaceFirst("^~", System.getProperty("user.home"));
		File file = new File(path);
		if (!file.exists()) {
			logger.error("File " + path + " did not exists! Execution will not work.");
			counter.correct--;
			counter.error++;
		}
		return path;
	}
	
	/**
	 * Checks if a directory ends with file separator, and in case, adds it;
	 * Checks if a directory exists in the file system, and in if this does not exist, returns the blocking error.
	 * @param dir the directory to check.
	 * @param logger the Log4j2 logger.
	 * @param counter the configuration counters (number of correct entries, number of warnings, number of errors).
	 * @return the directory checked.
	 */
	public static String checkDirectoryMandatory(String dir, Logger logger, ConfigurationCounters counter) {
		
		if(!dir.endsWith(String.valueOf("/"))){
				dir += "/";
		}
	
		if (dir.startsWith("~"))
			dir = dir.replaceFirst("^~", System.getProperty("user.home"));
		File file = new File(dir);
		if(!file.exists()){
			logger.error("Directory " + dir + " did not exist! Execution will not work.");
			counter.correct--;
			counter.error++;
		}
		return dir;
	}
	
	/**
	 * Checks if a directory (specified inside a JSONObject ends with file separator, and in case, adds it;
	 * Checks if a directory exists in the file system, and in if this does not exist, returns the blocking error.
	 * @param dir the directory to check.
	 * @param logger the Log4j2 logger.
	 * @param counter the configuration counters (number of correct entries, number of warnings, number of errors).
	 * @return the directory checked.
	 */
	public static String checkDirectoryMandatory(String dir, Logger logger, JSONObject containingObj, String parameter, ConfigurationCounters counter) {
		
		if(!dir.endsWith(String.valueOf("/"))){
				dir += "/";
		}
					
		File file = new File(dir);
		if(!file.exists()){
			logger.error("Directory " + dir + " did not exist! Execution will not work.");
			counter.correct--;
			counter.error++;
		}
		
		//Setting correct value also inside configuration
		containingObj.put(parameter, dir);
		
		return dir;
	}
	
	
	
	/**
	 * Checks if a directory ends with file separator, and in case, adds it;
	 * Checks if a directory exists in the file system, and in if this does not exist, returns a warning.
	 * @param dir the directory to check.
	 * @param logger the Log4j2 logger.
	 * @param counter the configuration counters (number of correct entries, number of warnings, number of errors).
	 * @return the directory checked.
	 */
	public static String checkDirectoryNotMandatory(String dir, Logger logger, ConfigurationCounters counter) {
	   	if(!dir.endsWith(String.valueOf("/"))){
				dir += "/";
		}
					
		File file = new File(dir);
		if(!file.exists()){
			logger.error("Directory " + dir + " did not exist! Please check your directory structure. The program will probably go in keepOut mode soon.");
			counter.correct--;
			counter.warning++;
		}
		return dir;
	}
	
	/**
	 * Gets the filename for the instance of this module.
	 * 
	 * @return the filename.
	 */
	public static String getFileName(String name, IConfig config) {
		return config.getString(Parameter.MODULE_NAME) + "-" + name + "-" + config.getString(Parameter.INSTANCE_NAME) + ".log";
	}
}
