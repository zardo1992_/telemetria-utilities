package com.nttdata.admeter.utils;

import java.nio.charset.Charset;

import com.nttdata.admeter.constant.Output;

public class OutputUtils {

	/**
	 * Gets the header from a line in BDB format.
	 * @param data the byte array of the data to parse.
	 * @param index the index of the header to retrieve.
	 * @return the String value of the header at the desired position, or an empty String if the index is too big.
	 */
	public static String getMessageHeader(byte[] data, int index) {
		try{
			String[] chunks = splitMessage(data);
			if(chunks != null && index < chunks.length){
				return chunks[index];
			}
			return "";	
		} catch(Exception e){
			return "";
		}
	}
		
	/**
	 * Splits a byte array in chuncks using a pattern.
	 * @param message the input array to be splitted.
	 * @return the array of chunks.
	 * @throws Exception if declared length and message length does not coincide.
	 */
	public static String[] splitMessage(byte[] message) throws Exception {
		String[] result = null;
		
		int pos = 0;
		int nextPos = 0;
		byte sep = Output.SEPARATOR.getBytes(Charset.forName("UTF-8"))[0];
		nextPos	= indexOf(message, sep, pos);
		if (nextPos > 0) {
			
			int length = Integer.parseInt(new String(message, pos, nextPos-pos));
			if(length != message.length)
				throw new Exception("Invalid message: length is " + message.length + " but first field value is " + length);
				
			pos = nextPos + 1;
			nextPos = indexOf(message, sep, pos);
			if (nextPos > 0) {
				int nFields = Integer.parseInt(new String(message, pos, nextPos-pos));
				result = new String[nFields];
				int[] offsets = new int[nFields];
				for (int i = 0; i < nFields && nextPos > 0 ; i++) {
					pos = nextPos + 1;
					nextPos = indexOf(message, sep, pos);
					if (nextPos > 0) {
						offsets[i] = Integer.parseInt(new String(message, pos, nextPos-pos));
					}
				}
				for(int i = 0; i < offsets.length && nextPos > 0; i++){
					pos = offsets[i];
					nextPos = indexOf(message, sep, pos);
					if(nextPos > 0){
						result[i] = new String(message, pos, nextPos-pos);
					} else { //Last field
						result[i] = new String(message, pos,  message.length - pos);
					}
				}
			}
		}
		
		return result;
	}
	
	/**
	 * Find the index of a byte in a byte array.
	 * @param array	the array to browse.
	 * @param val the value to look for.
	 * @param start starting position inside the array.
	 * @return a number >= 0 if the value was found, -1 otherwise.
	 */		
	public static int indexOf(byte[] array, byte val, int start) {
		int len;
		int pos;
		
		len = array.length;
		if ((start >= len) || (start < 0)) {
			return (-1);
		}
		for (pos=start; (pos<len) && (array[pos] != val); pos++);
		if (pos < len) return(pos);
		else return(-1);
	}
	
	public static boolean isEmpty(String text) {
		return (text == null) || (text.trim().length() == 0); 
	}
}
