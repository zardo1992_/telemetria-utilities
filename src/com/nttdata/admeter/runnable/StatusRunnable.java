package com.nttdata.admeter.runnable;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.apache.logging.log4j.Logger;

import com.nttdata.admeter.constant.ErrorCode;
import com.nttdata.admeter.constant.Parameter;
import com.nttdata.admeter.constant.Status;
import com.nttdata.admeter.constant.StatusTimestamp;
import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.interfaces.IStatusMonitor;
import com.nttdata.admeter.output.ErrorLogger;
import com.nttdata.admeter.utils.TimeUtils;

/**
 * A runnable class used to write the status of this module.
 * @author BurrafatoMa
 * @version 1.0.0
 *
 */
public class StatusRunnable extends BaseRunnable implements IStatusMonitor  {

	protected HashMap<String, StatusTimestamp> status;
	
	public StatusRunnable(IConfig jsonConfig, Logger logger, ErrorLogger errorLogger, long timeout) {
		super(jsonConfig, logger, errorLogger);
		
		long timestamp = System.currentTimeMillis();
		status = new HashMap<String, StatusTimestamp>();
		status.put(Status.INDEX_STATUS, new StatusTimestamp(timestamp, timeout));
	}
	
	public HashMap<String, StatusTimestamp> getStatusMap() { return this.status; }
	
	@Override
	public void run() {
		while (!Thread.currentThread().isInterrupted()) {
			try {
				setStatus(Status.INDEX_STATUS, System.currentTimeMillis());
				File file = getCurrentFile(Parameter.DIR_STATUS, "run");
				
				writeRunStatus(file);
	
				Thread.sleep(config.getLong(Parameter.MONITOR_STATUS_INTERVAL_SEC));
			} catch (IOException e) {
				logger.error("Error getting run file: " + e.getMessage());
				if(errorLogger != null)
					errorLogger.write(ErrorCode.FILE_NOT_FOUND, "[module:"+config.getString(Parameter.MODULE_NAME)+",instance:"+config.getString(Parameter.INSTANCE_NAME)+",method:StatusRunnable.run,exception:" + e.getMessage() + "]");
				setStatus(Status.INDEX_STATUS, -1L);
				Thread.currentThread().interrupt();
			} catch (InterruptedException e) {
				setStatus(Status.INDEX_STATUS, -1L);
				Thread.currentThread().interrupt();
			}
		}
	}
	
	/**
	 * Writes the run file.
	 * @param file a reference to the file to write.
	 */
	private synchronized void writeRunStatus(File file){
		long currentTime = System.currentTimeMillis();
		String currentStatus = "OK";
		StringBuilder statusString = new StringBuilder("Checking "+config.getString(Parameter.MODULE_NAME)+"-"+config.getString(Parameter.INSTANCE_NAME)+" run:[");
		
		Set<String> keys = status.keySet();
		Iterator<String> it = keys.iterator();
		while(it.hasNext()){
			String key = it.next();
			StatusTimestamp st = status.get(key);
			statusString.append(key+":lastTs=" + st.lastTs + ",timeout:" + st.timeout);
			if(currentTime - st.lastTs > st.timeout ){
				currentStatus = "KO";
			}
			if(it.hasNext()) statusString.append(";");
		}
		if(currentStatus.equals("OK")) logger.debug(currentStatus + " " + statusString + "]"); else logger.error(currentStatus + " " + statusString + "]");
		
		String line = 
			"{\"objlist\":[{\"timestamp\":\"" + 
			TimeUtils.long2String(errorLogger) + 
			"\",\"module\":\"" + 
			config.getString(Parameter.MODULE_NAME) + 
			"\",\"instance\":\"" + 
			config.getString(Parameter.INSTANCE_NAME) + 
			"\"}],\"run\":\"" + 
			currentStatus +
			"\"}\n";

		BufferedWriter bw = null;
		FileWriter fw = null;
		
		// Trick to avoid FileNotFoundException and File empty error in monitor:
		// Instead of writing the run file directly, first write a temp file
		// and then rename it (renaming is an atomic operation)
		File tempFile = new File(file.getAbsolutePath() + ".temp");
		try {
			fw = new FileWriter(tempFile, false);
	        bw = new BufferedWriter(fw);
	        bw.write(line);
	    } catch (IOException e){
	    	logger.error("Exception writing run status: ", e);
			errorLogger.write(ErrorCode.FILE_WRITE, "[module:"+config.getString(Parameter.MODULE_NAME)+",instance:"+config.getString(Parameter.INSTANCE_NAME)+",method:StatusRunnable.writeStatistics,exception:"+e.getMessage()+"]");
			setStatus(Status.INDEX_STATUS, -1L);
	    } finally {
	    	try{
	    		if(bw != null){
	    			bw.close();
	    			bw = null;
	    		}
	    	} catch(IOException e){
	    		logger.error("Exception closing buffer for run status: ", e);
				errorLogger.write(ErrorCode.FILE_WRITE, "[module:"+config.getString(Parameter.MODULE_NAME)+",instance:"+config.getString(Parameter.INSTANCE_NAME)+",method:StatusRunnable.writeStatistics,exception:"+e.getMessage()+"]");
				setStatus(Status.INDEX_STATUS, -1L);
	    	}
	    	try{
	    		if(fw != null){
	    			fw.close();
	    			fw = null;
	    		}
	    	} catch(IOException e){
	    		logger.error("Exception closing buffer for run status: ", e);
				errorLogger.write(ErrorCode.FILE_WRITE, "[module:"+config.getString(Parameter.MODULE_NAME)+",instance:"+config.getString(Parameter.INSTANCE_NAME)+",method:StatusRunnable.writeStatistics,exception:"+e.getMessage()+"]");
				setStatus(Status.INDEX_STATUS, -1L);
	    	}
	    	try{	    		
				if(!tempFile.renameTo(file)){
					// Workaround for Windows
				    Files.move(tempFile.toPath(), file.toPath(), java.nio.file.StandardCopyOption.REPLACE_EXISTING);
				}
	    	} catch(IOException e){
	    		logger.error("Exception closing buffer for run status: ", e);
				errorLogger.write(ErrorCode.FILE_WRITE, "[module:"+config.getString(Parameter.MODULE_NAME)+",instance:"+config.getString(Parameter.INSTANCE_NAME)+",method:StatusRunnable.writeStatistics,exception:"+e.getMessage()+"]");
				setStatus(Status.INDEX_STATUS, -1L);
	    	}
	    }
	}
	
	/**
	 * Sets the current status of this module.
	 * @param key the identifier of the thread.
	 * @param st an object containing the current timestamp if the thread is ok, -1 if error, and the timeout of this module, for initialization.
	 */
	@Override
	public synchronized void setStatus(String key, StatusTimestamp st) {
		status.put(key, st);		
	}

	/**
	 * Sets the current status of this module
	 * @param key the identifier of the thread
	 * @param timestamp the current timestamp if the thread is ok, -1 if error
	 */
	@Override
	public synchronized void setStatus(String key, long timestamp) {
		StatusTimestamp ts = status.get(key);
		if(ts != null){
			ts.lastTs = timestamp;
		} else {
			ts = new StatusTimestamp(timestamp, 60000);
		}
		status.put(key, ts);
	}

	public synchronized void updateTimeout() {
		setStatus(Status.INDEX_STATUS, new StatusTimestamp(System.currentTimeMillis(), config.getLong(Parameter.MONITOR_STATUS_INTERVAL_SEC)));
	}
	
	/**
	 * Removes the status of a thread from the monitoring.
	 * Only used to exit the "constant Keep Out mode", in case the configuration file has sever errors, and it finally gets fixed.
	 * @param key the key to remove.
	 */
	public synchronized void removeStatus(String key) {
		if(status.containsKey(key))
			status.remove(key);
	}
}
