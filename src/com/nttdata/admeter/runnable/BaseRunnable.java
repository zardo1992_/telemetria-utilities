package com.nttdata.admeter.runnable;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.Logger;

import com.nttdata.admeter.constant.Parameter;
import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.output.ErrorLogger;

/**
 * Base runnable class. Contains the basic methods used by the other runnables in order to write their files.
 * @author BurrafatoMa
 * @version 1.0.0
 *
 */
public class BaseRunnable implements Runnable {

	protected IConfig config;
	protected Logger logger;
	protected ErrorLogger errorLogger;
	
	public BaseRunnable(IConfig config, Logger logger, ErrorLogger errorLogger){
		this.config = config;
		this.logger = logger;
		this.errorLogger = errorLogger;
	}
	
	/**
	 * Called from the Main class when the the program is shut down.
	 */
	public void terminate() {
		Thread.currentThread().interrupt();
    }
	
	/**
	 * Gets the file to be written by the runnable class.
	 * @param type indicates if it is a run file or a stat file.
	 * @param extension the file extension.
	 * @return a reference to the file to be written.
	 * @throws IOException if the file cannot be found, opened, or created.
	 */
	protected File getCurrentFile(String type, String extension) throws IOException {
		String fileDir = config.getString(type);
		if(fileDir == null)
			throw new FileNotFoundException("Cannot find parameter " + type + " in config file");
		
		File directory = new File(fileDir);
		if(!directory.exists() || ! directory.isDirectory())
			throw new FileNotFoundException("Directory " + fileDir + " is invalid");
		
		String day = new SimpleDateFormat(config.getString(Parameter.STATUS_TS_FORMAT)).format(new Date());
		File file = new File(
				directory +
				File.separator +
				("run".equals(extension) ? "" : day ) +
				config.getString(Parameter.MODULE_NAME) +
				"-" +
				config.getString(Parameter.INSTANCE_NAME) +
				"." + extension
				);
		if(!file.exists())
			file.createNewFile();
		
		return file;
	}

	@Override
	public void run() {
		
	}
}
