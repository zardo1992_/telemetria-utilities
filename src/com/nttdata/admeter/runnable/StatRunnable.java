package com.nttdata.admeter.runnable;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.apache.logging.log4j.Logger;

import com.nttdata.admeter.constant.ErrorCode;
import com.nttdata.admeter.constant.Parameter;
import com.nttdata.admeter.constant.Status;
import com.nttdata.admeter.constant.StatusTimestamp;
import com.nttdata.admeter.interfaces.IConfig;
import com.nttdata.admeter.interfaces.IStatisticsMonitor;
import com.nttdata.admeter.output.ErrorLogger;

/**
 * A runnable class used to write the statistics of the modules.
 * @author BurrafatoMa
 * @version 1.0.0
 *
 */
public class StatRunnable extends BaseRunnable implements IStatisticsMonitor {

	protected String moduleName;
	protected String instanceName;
	protected String moduleVersion;
	
	protected StatusRunnable statusRunnable;
	
	protected HashMap<String, Integer> statistics;
	
	protected int loadFactorSum;
	protected int loadFactorNum;
	
	/**
	 * Constructor for the runnable that writes this module's statistics.
	 * @param config the configuration container object.
	 * @param logger the Log4j2 logger.
	 * @param errorLogger the error logger.
	 * @param statusRunnable the Runnable monitoring the status of all the program's threads.
	 * @param timeout the maximum timeout for this thread.
	 */
	public StatRunnable(IConfig config, Logger logger, ErrorLogger errorLogger, StatusRunnable statusRunnable, long timeout) {
		super(config, logger, errorLogger);
		
		moduleName = config.getString(Parameter.MODULE_NAME);
		instanceName = config.getString(Parameter.INSTANCE_NAME);
		moduleVersion = config.getString(Parameter.MODULE_VERSION);
		
		this.statusRunnable = statusRunnable;
		
		statusRunnable.setStatus(Status.INDEX_STATISTICS, new StatusTimestamp(System.currentTimeMillis(), timeout));
		this.statistics = new HashMap<String, Integer>();
		resetStatistics();
	}
	
	@Override
	public void run() {
		while (!Thread.currentThread().isInterrupted()) {
			logger.info("Checking " + moduleName + "-" + instanceName + " statistics...");
			try {
				statusRunnable.setStatus(Status.INDEX_STATISTICS, System.currentTimeMillis());
				writeStatistics(getCurrentFile(Parameter.DIR_STATISTICS, "sta"));
				// the reset must be done in the module writeStatistics(), in a synchronized way
				// resetStatistics();
				
			} catch (IOException e) {
				if(errorLogger != null)
					errorLogger.write(ErrorCode.FILE_NOT_FOUND, "[module:Parser,method:StatRunnable.run,exception:" + e.getMessage() + "]");
	    		logger.error("Error getting stat file: " + e.getMessage());
	    		statusRunnable.setStatus(Status.INDEX_STATISTICS, -1L);
				Thread.currentThread().interrupt();
			} finally {
				try {
					Thread.sleep(config.getLong(Parameter.MONITOR_STAT_INTERVAL_SEC));					
				} catch (InterruptedException e) {
					statusRunnable.setStatus(Status.INDEX_STATISTICS, -1L);
					Thread.currentThread().interrupt();
				}
			}
		}		
	}
	
	protected synchronized void writeStatistics(File file) {
		
	}
	
	protected synchronized void resetStatistics() {
		loadFactorSum = 0;
		loadFactorNum = 0;
	}
	
	/**
	 * Get a statistic's value.
	 */
	@Override
	public synchronized int getInt(String key) {
		if(statistics.containsKey(key))
			return statistics.get(key);
		else
			return 0;
	}
	
	/**
	 * Set a statistic's value.
	 */
	@Override
	public synchronized void putInt(String key, int value) {
		statistics.put(key, value);
	}
	
	/**
	 * Increment a statistic's value by one single point.
	 */
	@Override
	public synchronized void incrementInt(String key) {
		if(statistics.containsKey(key))
			statistics.put(key, statistics.get(key) + 1);
		else
			statistics.put(key, 1);
	}
	
	/**
	 * Increment a statistic's value by a number of points.
	 */
	@Override
	public synchronized void incrementInt(String key, int number) {
		if(statistics.containsKey(key))
			statistics.put(key, statistics.get(key) + number);
		else
			statistics.put(key, number);
	}
	
	/**
	 * Changes the statistics thread's timeout. It follows a configuration update.
	 */
	public synchronized void updateTimeout() {
		statusRunnable.setStatus(Status.INDEX_STATISTICS, new StatusTimestamp(System.currentTimeMillis(), config.getLong(Parameter.MONITOR_STAT_INTERVAL_SEC)));
	}

	/**
	 * Increments the load factor counters.
	 * This function is used only by the Parser and Writer modules.
	 * @param loadFactor the current load factor of the buffer reader.
	 */
	public synchronized void updateLoadFactor(long loadFactor) {
		loadFactorSum += loadFactor;
		loadFactorNum++;
	}
}
