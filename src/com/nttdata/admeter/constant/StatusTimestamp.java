package com.nttdata.admeter.constant;

import com.nttdata.admeter.runnable.StatusRunnable;

/**
 * Container object used by the {@link StatusRunnable} to indicate the status of a thread.
 * @author BurrafatoMa
 *
 */
public class StatusTimestamp {
	public StatusTimestamp(long timestamp, long timeout) {
		lastTs = timestamp;
		this.timeout = timeout + 5000; // Added delta to avoid possible small delays updating the timestamp
	}
	/**
	 * Last timestamp written by a thread.
	 */
	public long lastTs;
	
	/**
	 * Max difference between the current timestamp and the last timestamp written by a thread.
	 */
	public long timeout;
}
