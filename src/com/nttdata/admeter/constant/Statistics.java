package com.nttdata.admeter.constant;

import com.nttdata.admeter.runnable.StatRunnable;

/**
 * Class containing various String codes, used from the {@link StatRunnable} for its monitoring.
 * @author BurrafatoMa
 * @version 1.0.0
 *
 */
public class Statistics {
	
	/* Commons */
	
	/**
	 * Module name (Collector, Parser, Writer).
	 */
	public static final String MODULE_NAME 				= "moduleName";
	/**
	 * Instance Name.
	 */
	public static final String INSTANCE_NAME 			= "instanceName";
	/**
	 * Version number of this program.
	 */
	public static final String MODULE_VERSION 			= "moduleVersion";
	/**
	 * Number of messages from input discarded for any reasons.
	 */
	public static final String MESSAGES_DISCARDED 		= "msgDiscarded";
	/**
	 * Number of buffers written to output file.
	 */
	public static final String BUFFER_WRITE 			= "bufferWriteProcessed";
	/**
	 * Total number of generic program errors.
	 */
	public static final String GENERIC_ERRORS 			= "numGenericError";
	
	/* Collector */
	
	/**
	 * Total number of connections the client established with this servlet.
	 */
	public static final String CONNECTION_TOT 			= "totConnection";
	
	/**
	 * Number of POST requests (correctly or not) processed.
	 */
	public static final String PROCESSED_POST 			= "postProcessed";
	
	/* Parser */
	
	/**
	 * Miminum delay from message received by the collector, to the actual computation by the parser.
	 */
	public static final String PROCESSING_DELAY_MIN		= "minDelayProcessingMillis";
	/**
	 * Maxinum delay from message received by the collector, to the actual computation by the parser.
	 */
	public static final String PROCESSING_DELAY_MAX		= "maxDelayProcessingMillis";
	/**
	 * Number of messages from input correctly proccessed.
	 */
	public static final String MESSAGES_PROCESSED		= "msgProcessed";
	/**
	 * Number of events with unknown speed
	 */
	public static final String UNKNOWN_SPEED 			= "unknownSpeed";
	/**
	 * Number of events with unknown channel
	 */
	public static final String UNKNOWN_CHANNEL 			= "unknownChannel";
	/**
	 * Number of events with unknown campaign
	 */
	public static final String UNKNOWN_CAMPAIGN			= "unknownCampaign";
	/**
	 * Total number of events processed.
	 */
	public static final String EVENTS_PROCESSED			= "totalEventProcessed";
	/**
	 * Total number of events discarded.
	 */
	public static final String EVENTS_DISCARDED			= "totalEventDiscarded";
	/**
	 * Number of buffers read from the input file.
	 */
	public static final String BUFFER_READ 				= "bufferReadProcessed";
		
	/* Writer */
	
	/**
	 * Number of messages read from input and written to database.
	 */
	public static final String RECORDS_PROCESSED 		= "recordProcessed";
	/**
	 * Number of records discarded from writing on database.
	 */
	public static final String RECORDS_DISCARDED		= "recordDiscarded";
	/**
	 * Time employed to read the last group of lines.
	 */
	public static final String PROCESSING_TIME 			= "processingTimeMillis";
}
