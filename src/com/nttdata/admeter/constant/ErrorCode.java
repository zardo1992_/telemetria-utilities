package com.nttdata.admeter.constant;

/**
 * Class containing various String error codes, used to find the appropriate values in the error dictionary.
 * @author BurrafatoMa
 * @version 1.0.0
 *
 */
public class ErrorCode {
	public static class Source {
		public static final String COMMON = "COMMON";
		public static final String ADSMART = "ADSMART";
		public static final String ETHAN = "ETHAN";
	}
	
	//TODO ridefinire nuovo error dictionary per questo progetto
	public static final String GENERIC 					= "ERR0001";
	public static final String WATCHFILE_NOT_SET 		= "ERR0002";
	public static final String THREAD_STOP 				= "ERR0003";
	public static final String FILE_NOT_FOUND 			= "ERR0004";
	public static final String CONFIG_PARAM_INVALID 	= "ERR0005";
	public static final String DIR_CREATION 			= "ERR1006";
	public static final String FILE_WRITE 				= "ERR0007";
	public static final String TS_FORMAT_INVALID 		= "ERR0008";
	public static final String DB_CONNECTION 			= "ERR0009";
	public static final String ERRDICT_INVALID 			= "ERR0010";
	public static final String BUFFER_SMALL 			= "ERR0011";
	public static final String BUFFER_READ	 			= "ERR0012";
	public static final String DB_READ 					= "ERR0013";
	public static final String DB_WRITE 				= "ERR0014";
	public static final String DB_CONNECTION_RESTORED	= "ERR0015";
	public static final String CONFIG_FILE_DELETED 		= "ERR0016";
	public static final String CONFIG_FILE_RECREATED 	= "ERR0017";
	public static final String SOCKET_OPEN		 		= "ERR0018";
	public static final String SOCKET_SERVER_OPEN 		= "ERR0019";
	public static final String SOCKET_CLOSE	 			= "ERR0020";
	public static final String SOCKET_SERVER_CLOSE		= "ERR0021";
	public static final String SOCKET_CLOSED	 		= "ERR0022";
	public static final String SOCKET_SERVER_CLOSED 	= "ERR0023";
	public static final String SOCKET_WRITE		 		= "ERR0024";
	
	public static final String COLLECTOR_READY 			= "ERR1000";
	public static final String SOURCE_NOTFOUND 			= "ERR1001";
	public static final String USEREXTID_INVALID 		= "ERR1002";
	public static final String JSON_PARSE 				= "ERR1003";
	public static final String SOURCE_UNKNOWN 			= "ERR1004";
	public static final String URLDECODE 				= "ERR1005";
	public static final String EMPTY_BODY 				= "ERR1006";
	public static final String RUN_ERROR 				= "ERR1007";
	public static final String RUN_NOT_UPDATED 			= "ERR1008";
	public static final String BODY_READ 				= "ERR1009";
	public static final String FAILED_RETRIEVE_KEY		= "ERR1010";
	public static final String UNAVAILABLE_KEY			= "ERR1011";
	public static final String ENCRYPTION_FAILED		= "ERR1012";
	
	public static final String PARSER_READY 			= "ERR2000";
	public static final String SESSION_CLOSE_FAILED 	= "ERR2001";
	public static final String INPUT_EVENT_DISCARDED	= "ERR2002";
	public static final String ADSMART_UNEXPECTED 		= "ERR2003";
	public static final String DUPLICATED_EVENT 		= "ERR2004";
	public static final String MESSAGE_TS_NOTFOUND 		= "ERR2005";
	public static final String MESSAGE_TS_DELAY 		= "ERR2006";
	public static final String MESSAGE_TS_INVALID 		= "ERR2007";
	public static final String EVENTLIST_NOTFOUND 		= "ERR2008";
	public static final String EVENT_TS_INVALID 		= "ERR2009";
	public static final String EVENT_TS_NOTFOUND 		= "ERR2010";
	public static final String SERVICETYPE_NOTFOUND 	= "ERR2011";
	public static final String UNKNOWN_CAMPAIGN 		= "ERR2012";
	public static final String SERVICETYPE_INVALID 		= "ERR2013";
	public static final String MESSAGE_INVALID 			= "ERR2014";
	public static final String UNKNOWN_SPEED			= "ERR2015";
	public static final String SESSION_SEARCH 			= "ERR2016";
	public static final String SESSION_NOTFOUND 		= "ERR2017";
	public static final String SESSION_RECOVERED 		= "ERR2018";
	public static final String PAYLOAD_INVALID 			= "ERR2019";
	public static final String MESSAGE_TS_UPDATE 		= "ERR2020";
	public static final String DEVICEINFO_INVALID 		= "ERR2021";
	public static final String EVENT_NAME_NOTFOUND 		= "ERR2022";
	public static final String KPIG01_EXPIRED 			= "ERR2023";
	public static final String SESSION_EXPIRED 			= "ERR2024";
	public static final String LAYERFROMURI_HLS			= "ERR2025";
	public static final String LAYERFROMURI_HSS			= "ERR2026";
	public static final String SESSION_WITHOUTSTART 	= "ERR2027";
	public static final String SESSION_NO 				= "ERR2028";
	public static final String SESSION_ALREADYSTARTED 	= "ERR2029";
	public static final String VALID_RANGE_NOT_FOUND	= "ERR2030";
	public static final String XML_NOT_VALID			= "ERR2031";
	public static final String ANTICIPATED_EVENT		= "ERR2032";
	public static final String CHANNEL_NOT_FOUND		= "ERR2033";
	public static final String STAND_BY_OUT_UNEXPECTED	= "ERR2034";
	public static final String SURF_UNEXPECTED			= "ERR2035";
	public static final String CV_UNEXPECTED			= "ERR2036";
	public static final String VOD_UNEXPECTED			= "ERR2037";

	
	
	public static final String WRITER_READY 			= "ERR3000";
	public static final String BDB_ENV	 				= "ERR3001";
	public static final String BDB_OPEN 				= "ERR3002";
	public static final String BDB_WRITE 				= "ERR3003";
	public static final String BDB_CLOSE 				= "ERR3004";
	public static final String INPUT_LINE_INVALID		= "ERR3005";
	public static final String CANNOT_RETRIEVE_TS		= "ERR3006";
	
	
	/* ---  External data interface - M4  --- */
	public static final String EDI_READY 				= "ERR4000";
	public static final String EDI_ERROR_POST 			= "ERR4006";
	public static final String EDI_JSON_PARSE 			= "ERR4003";
	
	
	public static final String EXCEPTION_UNKNOWN 		= "ERR9999";
}
