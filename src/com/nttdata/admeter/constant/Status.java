package com.nttdata.admeter.constant;

import com.nttdata.admeter.runnable.StatusRunnable;

/**
 * Class containing various String codes, used from the {@link StatusRunnable} for its monitoring.
 * @author BurrafatoMa
 * @version 1.0.0
 *
 */
public final class Status {

	/* Constants to be written in the output .run file */
	
	public final static String STATUS_TIMESTAMP 		= "timestamp";
	public final static String STATUS_MODULE 			= "module";
	public final static String STATUS_INSTANCE 			= "instance";
	public final static String STATUS_STATUS 			= "run";
	
	/* Constants indicating the status of the module */
	public static final int STATUS_KO = 0;
	public static final int STATUS_OK = 1;
	
	/* Indexes used to distinguish between the various threads */
	
	public static final String INDEX_BUFFERWRITER 		= "buffer_";
	public static final String INDEX_STATISTICS 		= "stat";
	public static final String INDEX_STATUS 			= "run";
	public static final String INDEX_MONITOR 			= "monitor";
	public static final String INDEX_MESSAGEPARSER 		= "messageParser";
	public static final String INDEX_CONTEXTLISTENER_FIXED 		= "contextListenerFixed";
	public static final String INDEX_CONTEXTLISTENER_RUNTIME 	= "contextListenerRuntime";
	public static final String INDEX_PARSERMAIN			= "parserMain";
	public static final String INDEX_WRITERMAIN			= "writerMain";
	public static final String INDEX_WRITER 			= "writer";
}
