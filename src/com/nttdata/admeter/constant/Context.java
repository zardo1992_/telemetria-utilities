package com.nttdata.admeter.constant;

/**
 * Class containing various String constants used to retrieve the parameters from the web.xml file, and subsequently to reference them in the servlet context.
 * @author BurrafatoMa
 * @version 1.0.0
 *
 */
public class Context {

	public final static String LOGGER 				= "__LOGGER__";
	public final static String ERROR_LOGGER 		= "__ERROR_LOGGER__";
	public final static String WEBAPP_PROPERTIES	= "__WEBAPP_PROPERTIES__";
	public final static String WEBAPP_PROPERTIES_ADSMART 		= "__WEBAPP_PROPERTIES_ADSMART__";
	public final static String WEBAPP_PROPERTIES_ETHAN 	= "__WEBAPP_PROPERTIES_ETHAN__";
	public final static String ERROR_DICTIONARY 	= "__ERROR_DICTIONARY__";
	public final static String MODULE_STATISTICS 	= "__MODULE_STATISTICS__";
	public final static String MODULES_STATUS_ADSMART 		= "__MODULES_STATUS_ADSMART__";
	public final static String MODULES_STATUS_ETHAN 		= "__MODULES_STATUS_ETHAN__";
	
	public static final String STATISTICS_MONITOR 	= "statistics_monitor";
	public static final String STATUS_MONITOR 		= "status_monitor";
	
	//public static final String STATISTICS_MONITOR_ETHAN 	= "statistics_monitor_ethan";
	//public static final String STATUS_MONITOR_ETHAN 		= "status_monitor_ethan";
	
	public static final String CONFIG 				= "config";
	public static final String XML_PROPERTIES 		= "webapp-properties";
	public static final String MODULE_STATUS 		= "__MODULE_STATUS__";
	public static final String MAP_FILEWRITER_ADSMART 		= "__MAP_FILEWRITER_ADSMART__";
	public static final String MAP_FILEWRITER_ETHAN 		= "__MAP_FILEWRITER_ETHAN__";
	
	public static final String LOG4J_CONTEXT_NAME 	= "log4jContextName";
	public static final String LOG4J_CONFIGURATION	= "log4jConfiguration";
}
