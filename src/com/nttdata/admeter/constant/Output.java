package com.nttdata.admeter.constant;

/**
 * Output parameter configuration that have to be hardcoded (they cannot be set from the external configuration file). 
 * @author BurrafatoMa
 *
 */
public class Output {

	/**
	 * Timestamp format used for every log, error log, json timestamp.
	 */
	public static final String TIMESTAMP_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
	
	public static final String TIMESTAMP_UTC_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
	public static final String TIMESTAMP_LOCAL_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
	public static final String TIMESTAMP_MINUTE_FORMAT = "yyyy-MM-dd'T'HH:mm";
	/**
	 * Timestamp format used to create the BDB directory structure.
	 */
	public static final String TIMESTAMP_DB_FORMAT = "yyyyMMdd/HH00";
	/**
	 * Separator used to write in intermediate M1 and M2 output files.
	 */
	public static final String SEPARATOR = "|";
	/**
	 * Separator used to read from intermediate M1 and M2 output files.
	 */
	public static final String SEPARATOR_REGEX = "\\|";
}
