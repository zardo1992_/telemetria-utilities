package com.nttdata.admeter.constant;

/**
 * Class containing various String values that can be found in the event's json.
 * @author BurrafatoMa
 * @version 1.0.0
 *
 */
public class Json {
	
	//TODO ridefinire costanti di input, cosi' come presenti nell'xml anziche' nel json
	public final static String MESSAGE_TIMESTAMP = "msg_ts";
	public final static String MESSAGE_TIMESTAMP_TYPO = "mgs_ts"; // Used to handle the typo in the arriving json
	
	public static final String SOURCE = "source";
	
	public static final String DEVICE_INFO = "device_info";
	public static final String DEVICE_INFO_DEVID = "dev_id";
	public static final String DEVICE_INFO_DEVUUID = "dev_uuid";
	public static final String DEVICE_INFO_DEVFAMILY = "dev_family";
	public static final String DEVICE_INFO_USEREXTID = "user_extid";
	public static final String DEVICE_INFO_USERAGENT = "user_agent";
	public static final String DEVICE_INFO_CONN = "conn";
	
	public static final String DEVICE_ID = "user_extid";
	
	public static final String EVENT_LIST = "events_list";
	public static final String EVENT_TS = "ts";
	public static final String EVENT_NAME = "name";
	public static final String EVENT_PAYLOAD = "payload";
	
	/**
	 * Session Start CHannel.
	 */
	public static final String EVENT_NAME_SSCH = "SSCH";

	/**
	 * Session Start Video On Demand.
	 */
	public static final String EVENT_NAME_SSVOD = "SSVOD";
	
	/**
	 * Session sTreaming Download.
	 */
	public static final String EVENT_NAME_STD = "STD";

	/**
	 * Session sTreaming Pause.
	 */
	public static final String EVENT_NAME_STP = "STP";
	
	/**
	 * Session sTreaming Restart.
	 */
	public static final String EVENT_NAME_STR = "STR";
	
	/**
	 * Session sTreaming Change Level.
	 */
	public static final String EVENT_NAME_STCL = "STCL";
	
	/**
	 * Session sTreaming ReBuffering.
	 */
	public static final String EVENT_NAME_STRB = "STRB";
	
	/**
	 * Session Error.
	 */
	public static final String EVENT_NAME_SE = "SE";
	
	/**
	 * Session Close.
	 */
	public static final String EVENT_NAME_SC = "SC";
	
	/**
	 * Application Startup Information.
	 */	
	public static final String EVENT_NAME_ASI = "ASI";
	
	/**
	 * Application Close Information.
	 */
	public static final String EVENT_NAME_ACI = "ACI";
	
	/**
	 * Application Startup Information.
	 */	
	public static final String EVENT_NAME_SSTT = "SSTT";
	
	/**
	 * Application Close Information.
	 */
	public static final String EVENT_NAME_KPIG01 = "KPIG01";
	
	public static final String EVENT_PAYLOAD_SESSIONID = "session_id";
	public static final String EVENT_PAYLOAD_STARTTIME = "start_time";
	public static final String EVENT_PAYLOAD_DRMTIME = "drm_time";
	public static final String EVENT_PAYLOAD_BUFFTIME = "buffering_time";
	public static final String EVENT_PAYLOAD_PBSTTIME = "playback_start_time";
	public static final String EVENT_PAYLOAD_IPSERVER = "ip_server";
	public static final String EVENT_PAYLOAD_MANURI = "manifest_uri";
	public static final String EVENT_PAYLOAD_MANDLBYTE = "manifest_dwnl_byte";
	public static final String EVENT_PAYLOAD_MANDLTIME = "manifest_dwnl_time";
	public static final String EVENT_PAYLOAD_HTTPRESP = "http_response";
	public static final String EVENT_PAYLOAD_CHID = "channel_id";
	public static final String EVENT_PAYLOAD_CHTYPE = "channel_type";
	public static final String EVENT_PAYLOAD_CHEPG = "channel_epg";
	public static final String EVENT_PAYLOAD_CHNAME = "channel_name";
	public static final String EVENT_PAYLOAD_CHNAME_TYPO = "channle_name"; // Used to handle the typo in the arriving json
	public static final String EVENT_PAYLOAD_OFFERID = "offer_id";
	public static final String EVENT_PAYLOAD_ASTITLE = "asset_title";
	public static final String EVENT_PAYLOAD_ASTYPE = "asset_type";
	public static final String EVENT_PAYLOAD_ASSOURCE = "asset_source";
	public static final String EVENT_PAYLOAD_LAYER = "Layer";
	public static final String EVENT_PAYLOAD_BUFFSIZE = "buffer_size";
	public static final String EVENT_PAYLOAD_FPSDEC = "fps_decoded";
	public static final String EVENT_PAYLOAD_CHUNKTYPE = "chunk_type";
	public static final String EVENT_PAYLOAD_CHUNKINDEX= "chunk_index";
	public static final String EVENT_PAYLOAD_CHUNKURI = "chunk_uri";
	public static final String EVENT_PAYLOAD_RESPTIME = "response_time";
	public static final String EVENT_PAYLOAD_DLBYTE = "dwnl_byte";
	public static final String EVENT_PAYLOAD_DLTIME = "dwnl_time";
	public static final String EVENT_PAYLOAD_PAUSETIME = "pause_time";
	public static final String EVENT_PAYLOAD_RESTARTTIME = "restart_time";
	public static final String EVENT_PAYLOAD_REBUFFSTART = "rebuffering_start_time";
	public static final String EVENT_PAYLOAD_REBUFFEND = "rebuffering_end_time";
	public static final String EVENT_PAYLOAD_BITRATEFROM = "bitrate_from";
	public static final String EVENT_PAYLOAD_BITRATETO = "bitrate_to";
	public static final String EVENT_PAYLOAD_CLOSETIME = "closing_time";
	public static final String EVENT_PAYLOAD_ERRTEXT = "error_text";
	public static final String EVENT_PAYLOAD_ERRTYPE = "error_type";
	public static final String EVENT_PAYLOAD_ERRCODE = "error_code";
	public static final String EVENT_PAYLOAD_EVENTID = "event_id";
	public static final String EVENT_PAYLOAD_VODID = "vod_id";
	public static final String EVENT_PAYLOAD_SLVER = "silverlight_version";
	public static final String EVENT_PAYLOAD_EVENTNAME = "event_name";
	public static final String EVENT_PAYLOAD_ERRMSG = "error_message";
	public static final String EVENT_PAYLOAD_DEVICEVEN = "device_vendor";
	public static final String EVENT_PAYLOAD_DEVICEMOD = "device_model";
	public static final String EVENT_PAYLOAD_DEVICESO = "device_so";
	public static final String EVENT_PAYLOAD_CODCLI = "codice_cliente";
	public static final String EVENT_PAYLOAD_STOPTIME = "stop_time";
}
