package com.nttdata.admeter.constant;

/**
 * Class containing various String keys that can be found in the application configuration file.
 * @author BurrafatoMa
 * @version 1.0.1
 *
 */
public class Parameter {

	final static
	public String SOURCE = "_source_";
	
	final static
	public String INSTANCE_NAME = "instanceName";
	
	/* fixed */
	final static 
	public String SECTION_FIXED = "fixed";
	/* commonCfg */
	final static 
	public String	
		SUB_SECTION_COMMON = "commonCfg",
			DIR_STATUS = "runFileDir",
			DIR_STATISTICS = "statFileDir",
			STATUS_TS_FORMAT = "statFileNameTsFormat",
			ERROR_FILE_DIR = "errorFileDir",
			ERROR_TS_FORMAT = "errorFileNameTsFormat",
			ERROR_DICTIONARY_FILE_DIR = "errorDictionaryFileDir",
			ERROR_DICTIONARY_FILE_NAME = "errorDictionaryFileName",
			SESSIONDB_IP = "databaseIp",
			LOOP_FS = "countFileSystemFail",
			LOOP_OS = "countOffsetFail",
			TS_HEADER_TYPE = "timestampHeaderType",
			DATABASE_READ_RETRY_NUM = "databaseReadRetryNum",
	        DATABASE_READ_WAIT_MSEC = "databaseReadWaitMsec",
	        DATABASE_WRITE_RETRY_NUM = "databaseWriteRetryNum",
	        DATABASE_WRITE_WAIT_MSEC = "databaseWriteWaitMsec";
	
	/* adsmartCfg */
	final static 
	public String	
		SUB_SECTION_ADSMART = "adsmartCfg",
			PARALLEL_THREADS_NUM = "parallelThreadsNum",
			INPUT_FILE_DIR = "inputFilesDir",
			INPUT_FILE_PATTERN = "inputFilesPattern",
			OUTPUT_EVENT_FILE_DIR = "outputEventsFileDir",
			OUTPUT_EVENT_FILE_NAME = "outputEventsFileName",
			OUTPUT_TS_FORMAT = "outputFileNameTsFormat",
			OVERRIDE_CHANNEL_FILE_DIR = "overrideChannelFileDir",
			OVERRIDE_CHANNEL_FILE_NAME = "overrideChannelFileName",
			OVERRIDE_CAMPAIGN_FILE_DIR = "overrideCampaignFileDir",
			OVERRIDE_CAMPAIGN_FILE_NAME = "overrideCampaignFileName",
			TBL_SPEED_FILE_DIR = "tblSpeedFileDir",
			TBL_SPEED_FILE_NAME = "tblSpeedFileName",
			XML_SCHEMA_FILE_DIR = "xmlSchemaFileDir",
			XML_SCHEMA_FILE_NAME = "xmlSchemaFileName",
			OFFSET_FILE_DIR = "ofsFileDir",
			OFFSET_FILE_NAME = "ofsFileName",
			HEADER_ID = "headerIdentifier";
	
	final static
	public String 
		DB_TABLE_CAMPAIGN_LIST = "dbTableCampaignList",
			BUCKET_NAME = "bucketName",
			BUCKET_PASS = "bucketPass",
			ALL_CAMPAIGNS_VIEW = "allCampaignsView",
			ALL_CAMPAIGNS_VIEW_DD = "allCampaignsViewDD";
	
	final static
	public String 
		DB_TABLE_CAMPAIGN_HISTORY = "dbTableCampaignHistory",
			ALL_CAMPAIGNS_HISTORY_VIEW = "allCampaignsHistoryView",
			ALL_CAMPAIGNS_HISTORY_VIEW_DD = "allCampaignsHistoryViewDD";
	
	final static
	public String 
		DB_TABLE_CHANNEL_LIST = "dbTableChannelList",
			ALL_CHANNEL_VIEW = "allChannelsView",
			ALL_CHANNEL_VIEW_DD = "allChannelsViewDD";
	
	final static
	public String 
		DB_TABLE_STATE_MACHINE = "dbTableStateMachine";
	
	
	/* ethanCfg */
	final static 
	public String	
		SUB_SECTION_ETHAN = "ethanCfg";
	
	/* runtime */
	final static 
	public String SECTION_RUNTIME = "runtime",
			STATUS_RECOVERY = "statusRecovery",
			MONITOR_STAT_INTERVAL_SEC = "monitorStatIntervalSec",
			MONITOR_STATUS_INTERVAL_SEC = "monitorRunIntervalSec",
			OFFSET_CYCLE_READ_INTERVAL = "ofsReadCycleIntervalMsec",
			TBL_CMP_FREQ_READ_MIN = "tblCampaignFreqReadMin",
			TBL_CMP_RETRY_NUM = "tblCampaignRetryNum",
			TBL_CMP_HISTORY_READ_SINCE_DAY = "tblCampaignHistoryReadSinceDay",
			TBL_CMP_LOG = "tblCampaignLog",
			TBL_CH_TIME_READ = "tblChannelTimeRead",
			TBL_CH_READ_SINCE_DAY = "tblChannelReadSinceDay",
			TBL_CH_RETRY_NUM = "tblChannelRetryNum",
			TBL_CH_LOG = "tblChannelLog",
			BUFFER = "bufferWriteCfg",
			BUFFER_SIZE = "ioBufferSizeMb",
			BUFFER_REFRESH = "ioBufferTimeMsec";
	
	
	/* commons */
	public static final String MODULE_NAME = "moduleName";
	public static final String MODULE_VERSION = "moduleVersion";
	
	public static final String DIR_ERROR = "errorFileDir";
	
	/* collector */
	public static final String CLIENT_IP = "getOriginIpMethod";
	
	public static final String SESSIONDB = "sessionDatabase";
	
	public static final String LOOP_DB = "countDbFail";
	
	/* writer */
	public static final String DB_HOME = "dbHomeDir";
	public static final String DB_NAME = "dbName";
	public static final String DB_CONFIG = "dbConfig";
	public static final String DB_CONFIG_CACHE = "cacheSize";
	public static final String DB_CONFIG_PAGE = "pageSize";

	public static final String DB_INDEX = "dbIndex";
	public static final String TS_POSITION = "dbPartTsPosition";
	public static final String DB_MAX_OPEN_ENV_NUM = "maxOpenEnvNum";
	public static final String DB_ENV_CLOSE_DIVISOR_NUM = "dbEnvCloseDivisorNum";
	
	/*  External Data Interface  */
	public static final String OUT_CHANNEL_DIR = "outputChannelFileDir";
	public static final String OUT_CHANNEL_TS_FORMAT = "outputChannelFileTsFormat";
	public static final String OUT_CHANNEL_FILENAME = "outputChannelFileName";
	
	public static final String COUCHDB_CONFIG = "sessionDatabase";
	
	
	/* collector */
	public static final String KEEPOUT_MODE = "keepOutMode";
	public static final String KEEPOUT_RESPONSE = "keepOutResponse";

	public final static String MONITOR = "checkCfg";
	public final static String MONITOR_NAME = "checkName";
	public final static String MONITOR_TOCHECK = "checkEnable";
	public final static String MONITOR_FILE_PATH = "runFilePath";
	public final static String MONITOR_FILE_NAME = "runFileName";
	public final static String MONITOR_INTERVAL = "checkIntervalSec";
	public final static String OUTPUT_FILE_DIR = "outputFileDir";
	public final static String OUTPUT_FILE_NAME = "outputFileName";
	public final static String ENCRYPTION_CFG = "EncryptionCfg";
	
	public static final String KEYSTORE_DIR = "keyStoreFileDir";
	public static final String KEYSTORE_NAME = "keyStoreFileName";
	public static final String KEYSTORE_RETRY_NUM = "keyStoreRetryNum";
	public static final String KEYSTORE_RETRY_SEC = "keyStoreRetryWaitSec";
	public static final String KEYSTORE_KEYNAME = "keyStoreKeyName";
	
	/* writer */	
	public static final String MAX_RECORDS_PER_CYCLES = "maxRecordsPerCycles";
	public static final String COUNT_BEFORE_CLOSE_ENVS_CYCLES = "countBeforeCloseEnvsCycles";
	
	/*  External Data Interface  */
	public static final String RETENTION_CFG = "retentionCfg";
	public static final String RETENTION_CAMPAIGN_HISTORY = "campaignHistoryRetentionDay";
	public static final String RETENTION_CHANNEL_HISTORY = "channelListRetentionDay";
	
	public static final String CHANNEL_LIST_TYPE = "channelListType";
	public static final String CHANNEL_LIST_DEFINITION = "channelListDefinition";
	public static final String CAMPAIGN_LIST_DEFINITION = "campaignListDefinition";
	
}
