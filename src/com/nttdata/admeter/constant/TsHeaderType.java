package com.nttdata.admeter.constant;

/**
 * Class defining some constant relative to the type of timestamp the Parser has to write in the header of its output.
 * @author BurrafatoMa
 *
 */
public class TsHeaderType {
	/**
	 * The timestamp in the header must be the same timestamp of the message.
	 */
	public static final String MSG = "MSG";
	
	/**
	 * The timestamp in the header must be the machine time when the parser wrote this line.
	 */
	public static final String WR = "WR";
}
