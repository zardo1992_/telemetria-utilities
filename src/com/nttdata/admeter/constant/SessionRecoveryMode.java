package com.nttdata.admeter.constant;

/**
 * Class containing some constants indicating how the application must interact with Couchbase to store and retrieve the user sessions.
 * @author BurrafatoMa
 *
 */
public class SessionRecoveryMode {
	
	/**
	 * The database is not used.
	 */
	public static final int OFF = 0;
	/**
	 * If a specific session cannot be found in the JVM memory, then it is searched into the database.
	 */
	public static final int SHARED = 1;
	/**
	 * All sessions of a user are retrieved from the dataabase at every message.
	 * Then, every session in the JVM memory is compared to the ones from the database.
	 * If the session is not present in the JVM or if its version is older, then the database info is loaded into the JVM memory.
	 */
	public static final int VERSION = 2;
}
